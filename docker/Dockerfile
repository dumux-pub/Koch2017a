# Koch2017a docker container
# see https://github.com/phusion/baseimage-docker for information on the base image
# It is Ubuntu LTS customized for better Docker compatibility
FROM phusion/baseimage:0.9.22
MAINTAINER timo.koch@iws.uni-stuttgart.de

# run Ubuntu update as advised on https://github.com/phusion/baseimage-docker
RUN apt-get update \
    && apt-get upgrade -y -o Dpkg::Options::="--force-confold" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install the basic dependencies
RUN apt-get update \
    && apt-get install --no-install-recommends --yes \
    ca-certificates \
    vim \
    python-dev \
    python-pip \
    git \
    pkg-config \
    build-essential \
    gfortran \
    cmake \
    mpi-default-bin \
    mpi-default-dev \
    libsuitesparse-dev \
    libsuperlu-dev \
    libeigen3-dev \
    doxygen \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# add the permission helper script to the my_init service
COPY ./docker/setpermissions.sh /etc/my_init.d/setpermissions.sh

# create a dumux user
# add the welcome message (copied further down) output to bashrc
# make the set permission helper script executable
# add user to video group which enables graphics if desired
RUN useradd -m --home-dir /dumux dumux \
    && echo "cat /dumux/WELCOME" >> /dumux/.bashrc \
    && chmod +x /etc/my_init.d/setpermissions.sh \
    && usermod -a -G video dumux

# Turn off verbose syslog messages as described here:
# https://github.com/phusion/baseimage-docker/issues/186
RUN touch /etc/service/syslog-forwarder/down

# copy the extracted dumux-pub module and make dumux own it
COPY . /dumux/Koch2017a
RUN chown -R dumux:dumux /dumux/Koch2017a

# switch to the dumux user and set the working directory
USER dumux
WORKDIR /dumux

# create a shared volume communicating with the host
RUN mkdir /dumux/shared
VOLUME /dumux/shared

# This is the message printed on entry
COPY ./docker/WELCOME /dumux/WELCOME

# install dumux-pub module dependencies
COPY installKoch2017a.sh /dumux/installKoch2017a.sh
RUN ./installKoch2017a.sh && rm -f /dumux/installKoch2017a.sh

# configure module
RUN /dumux/dune-common/bin/dunecontrol --opts=/dumux/dumux/optim.opts all

# build doxygen documentation and tests
# all applications that use dune_add_test will be built like this
RUN cd Koch2017a/build-cmake && make doc && make -j4 build_tests

# switch back to root
USER root

# set entry point like advised https://github.com/phusion/baseimage-docker
# this sets the permissions right, see above
ENTRYPOINT ["/sbin/my_init","--quiet","--","/sbin/setuser","dumux","/bin/bash","-l","-c"]

# start interactive shell
CMD ["/bin/bash","-i"]

