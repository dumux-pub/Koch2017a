<img src="doc/dumuxroot.png" alt="dumux root logo" width="200"/>

Welcome to the dumux pub table Koch2017a (dumux-root)
======================================================

This module contains the source code for the four examples in the
paper

__T. Koch, K. Heck, N. Schröder, H. Class, R. Helmig, (2018)__, [_A new model concept for plant-scale soil-root interaction including soil evaporation,
growth and transport processes._ (doi: 10.2136/vzj2017.12.0210)](https://doi.org/10.2136/vzj2017.12.0210) 

For a shorter description, also see the [CSA News article (doi:10.2134/csa2018.63.1110)](https://dl.sciencesocieties.org/publications/csa/pdfs/63/11/10).

__edit__ If you want to reproduce the paper results use this repository. If you want to work with root models in DuMu<sup>x</sup> check out the newest
DuMu<sup>x</sup> version at (https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git) which includes and updated and better documented version
of the root-soil interaction models (an updated version of example of the tracer example from the paper can be found [here](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/tree/master/test/multidomain/embedded/1d3d/1p2c_richards2c).

For installation with Docker (recommended) read the next section. For building from source
clone this repository in your favourite location
```bash
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Koch2017a.git
```

Download the dependencies
```bash
./Koch2017a/installKoch2017a.sh
```

Configure and build libraries
```bash
./dune-common/bin/dunecontrol --opts=./dumux/optim.opts all
```

There are four examples in the folders, or in subfolders of those

```
Koch2017a/build-cmake/appl/root/precipitation
Koch2017a/build-cmake/appl/root/evaporation
Koch2017a/build-cmake/appl/root/growth
Koch2017a/build-cmake/appl/root/tracer
```

All examples can be compiled by the following commands

```bash
cd Koch2017a/build-cmake
make -j4 build_tests
```

Run an example
```bash
cd appl/root/tracer
./test_rosi2c
```

Using the pub table Koch2017a with docker
===========================================

Using the pub table with Docker is especially easy.
It should work on Linux, Windows, macOS, but was only
tested on Linux so far.

Create a new folder in your favourite location and change into it
```bash
mkdir dumux
cd dumux
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Koch2017a/raw/master/docker/pubtable_koch2017a
```

Make it executable
```bash
chmod +x pubtable_koch2017a
```

Open the pub table
```bash
./pubtable_koch2017a open
```

The first time this will download the latest image. This may take a while
depending on your internet conncetion. After the download finished
the container will spin up. It will mount the new "dumux"
directory into the container at /dumux/shared. Put files
in this folder to share them with the host machine. This
could be e.g. VTK files produced by the simulation that
you want to visualize on the host machine. To try that:

Run an example
```bash
cd Koch2017a/build-cmake/appl/root/tracer
./test_rosi2c
```

Copy the files to the shared folder an visualize them on the host machine
```bash
cp *vtu *vtp *pvd /dumux/shared
```

On the host machine in folder "dumux"
```bash
paraview *pvd
```
