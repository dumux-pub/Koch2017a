// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A sub problem for the 2p2c soil problem
 */
#ifndef DUMUX_ROSI_SOIL_PROBLEM_HH
#define DUMUX_ROSI_SOIL_PROBLEM_HH

#include <cmath>

#include <dumux/implicit/cellcentered/tpfa/properties.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>
#include <dumux/porousmediumflow/richards/implicit/model.hh>
// #include <dumux/material/components/simpleh2o.hh>
// #include <dumux/material/components/h2o.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

//! get the properties needed for subproblems
#include <dumux/mixeddimension/subproblemproperties.hh>

#include "evaporationtestspatialparams.hh"

#include <dumux/io/gnuplotinterface.hh>

namespace Dumux
{
template <class TypeTag>
class SoilTestProblem;

namespace Properties
{
#if RICHARDS
NEW_TYPE_TAG(SoilTestProblem, INHERITS_FROM(CCTpfaModel, Richards, TwoPTwoCTestSpatialParams));
#else
NEW_TYPE_TAG(SoilTestProblem, INHERITS_FROM(CCTpfaModel, TwoPTwoC, TwoPTwoCTestSpatialParams));
#endif

// Set the grid type
SET_TYPE_PROP(SoilTestProblem, Grid, Dune::YaspGrid<3, Dune::TensorProductCoordinates<double, 3> >);

SET_BOOL_PROP(SoilTestProblem, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(SoilTestProblem, EnableGlobalVolumeVariablesCache, true);
SET_BOOL_PROP(SoilTestProblem, EnableGlobalFluxVariablesCache, true);
SET_BOOL_PROP(SoilTestProblem, SolutionDependentAdvection, true);

// Set the problem property
SET_TYPE_PROP(SoilTestProblem, Problem, SoilTestProblem<TypeTag>);

// Set the spatial parameters
SET_TYPE_PROP(SoilTestProblem, SpatialParams, TwoPTwoCTestSpatialParams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(SoilTestProblem, ProblemEnableGravity, true);

// Enable velocity output
SET_BOOL_PROP(SoilTestProblem, VtkAddVelocity, false);

// Set the grid parameter group
SET_STRING_PROP(SoilTestProblem, GridParameterGroup, "SoilGrid");

// Choose pn and Sw as primary variables
#if !RICHARDS
SET_INT_PROP(SoilTestProblem, Formulation, TwoPTwoCFormulation::pwsn);
#endif

// Choose the h2o air fluid system with simple h2o water type
SET_TYPE_PROP(SoilTestProblem,
              FluidSystem,
              Dumux::FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar),
                                          Dumux::SimpleH2O<typename GET_PROP_TYPE(TypeTag, Scalar)>,
                                          /*useComplexrelations=*/true>);

// Use mass fractions
SET_BOOL_PROP(SoilTestProblem, UseMoles, false);

//! Determines whether the constraint solver is used
SET_BOOL_PROP(SoilTestProblem, UseConstraintSolver, true);

#if RICHARDS
SET_BOOL_PROP(SoilTestProblem, EnableWaterDiffusionInAir, true);
#endif

SET_BOOL_PROP(SoilTestProblem, UseKelvinEquation, true);
}

/*!
 * \ingroup OnePBoxModel
 * \ingroup ImplicitTestProblems
 * \brief  Test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 *
 * The domain is box shaped. All sides are closed (Neumann 0 boundary)
 * except the top and bottom boundaries (Dirichlet), where water is
 * flowing from bottom to top.
 *
 * In the middle of the domain, a lens with low permeability (\f$K=10e-12\f$)
 * compared to the surrounding material (\f$ K=10e-10\f$) is defined.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box1p -parameterFile test_box1p.input</tt> or
 * <tt>./test_cc1p -parameterFile test_cc1p.input</tt>
 *
 * The same parameter file can be also used for 3d simulation but you need to change line
 * <tt>typedef Dune::SGrid<2,2> type;</tt> to
 * <tt>typedef Dune::SGrid<3,3> type;</tt> in the problem file
 * and use <tt>1p_3d.dgf</tt> in the parameter file.
 */
template <class TypeTag>
class SoilTestProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    using ParentType = ImplicitPorousMediaProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using PointSource = typename GET_PROP_TYPE(TypeTag, PointSource);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using MaterialLaw = typename GET_PROP_TYPE(TypeTag, MaterialLaw);
    using SubControlVolumeFace = typename GET_PROP_TYPE(TypeTag, SubControlVolumeFace);
#if !ISOTHERMAL
    using ThermalConductivityModel = typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel);
#endif

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum
    {
        conti0EqIdx = Indices::conti0EqIdx,
#if !RICHARDS
        contiWEqIdx = Indices::contiWEqIdx,
        contiNEqIdx = Indices::contiNEqIdx,
#endif
#if !ISOTHERMAL
        energyEqIdx = Indices::energyEqIdx
#endif

    };
    enum
    {
        pressureIdx = Indices::pressureIdx,
#if !RICHARDS
        switchIdx = Indices::switchIdx,
#endif
#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx
#endif
    };
   enum
   {
       // the indices for the phase presence
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases
    };
    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using GlobalProblemTypeTag = typename GET_PROP_TYPE(TypeTag, GlobalProblemTypeTag);
    using CouplingManager = typename GET_PROP_TYPE(GlobalProblemTypeTag, CouplingManager);

    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    SoilTestProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        swInit_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InitialSoilWaterSaturationTop);
        swPrecip_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, PrecipitationSoilWaterSaturationTop);
        pcTop_ = MaterialLaw::pc(this->spatialParams().materialLawParamsAtPos(this->bBoxMax()), swInit_);
        pcPrecip_ = MaterialLaw::pc(this->spatialParams().materialLawParamsAtPos(this->bBoxMax()), swPrecip_);
        tPrecipStart_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, StartPrecipitation);
        precipDuration_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, LengthPrecipitation);
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "_soil";
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Return the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 15C

    /*!
     * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     */
    Scalar nonWettingReferencePressure() const
    { return 1.0e5; };

    /*!
     * \brief Applies a vector of point sources. The point sources
     *        are possibly solution dependent.
     *
     * \param pointSources A vector of PointSource s that contain
              source values for all phases and space positions.
     *
     * For this method, the \a values method of the point source
     * has to return the absolute mass rate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().bulkPointSources(); }

    /*!
     * \brief Evaluate the point sources (added by addPointSources)
     *        for all phases within a given sub-control-volume.
     *
     * This is the method for the case where the point source is
     * solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param pointSource A single point source
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub-control volume within the element
     *
     * For this method, the \a values() method of the point sources returns
     * the absolute rate mass generated or annihilate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        // compute source at every integration point
        const auto& bulkVolVars = this->couplingManager().bulkVolVars(source.id());
        const auto& lowDimVolVars = this->couplingManager().lowDimVolVars(source.id());

        const auto& spatialParams = this->couplingManager().lowDimProblem().spatialParams();
        const unsigned int lowDimElementIdx = this->couplingManager().pointSourceData(source.id()).lowDimElementIdx();
        const Scalar Kr = spatialParams.Kr(lowDimElementIdx);
        const Scalar rootRadius = spatialParams.radius(lowDimElementIdx);

        PrimaryVariables sourceValues(0.0);
        // advective fluxes, convert to molar fluxes if using mole fractions
        const Scalar waterMassSink = 2 * M_PI *rootRadius * Kr *(lowDimVolVars.pressure(wPhaseIdx) - bulkVolVars.pressure(wPhaseIdx))
                                       * bulkVolVars.density(wPhaseIdx);

        sourceValues[conti0EqIdx] = useMoles ? waterMassSink / FluidSystem::molarMass(wCompIdx) : waterMassSink;
        sourceValues[conti0EqIdx] *= source.quadratureWeight()*source.integrationElement();
 #if !ISOTHERMAL
        //convective heat transport
        if (sourceValues[conti0EqIdx] > 0){
            sourceValues[energyEqIdx] += sourceValues[conti0EqIdx] * lowDimVolVars.enthalpy(wPhaseIdx);
        }
        else{
            sourceValues[energyEqIdx] += sourceValues[conti0EqIdx] * bulkVolVars.enthalpy(wPhaseIdx);
        }
        //conduction
         sourceValues[energyEqIdx] += -2* M_PI *rootRadius *0.5* (bulkVolVars.temperature() - lowDimVolVars.temperature());
#endif
        source = sourceValues;
    }

    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;
        bcTypes.setAllNeumann();

        if (this->timeManager().time() + this->timeManager().timeStepSize() > tPrecipStart_
            && this->timeManager().time() + this->timeManager().timeStepSize() < tPrecipStart_+precipDuration_
            && globalPos[2] > this->bBoxMax()[2] - eps_)
            bcTypes.setAllDirichlet();

        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        if (globalPos[2] > this->bBoxMax()[2] - eps_)
        {
            PrimaryVariables values(0.0);
            const Scalar pw = 1.0e5 - pcPrecip_;
            values[pressureIdx] = pw;
#if !RICHARDS
            values[switchIdx] = 1.0 - swPrecip_;
#endif
            values.setState(bothPhases);
            return values;
        }
        else
        {
            return initialAtPos(globalPos);
        }
    }

    // \}
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
     */
    PrimaryVariables neumann(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        const Scalar t = this->timeManager().time() + this->timeManager().timeStepSize();

        //plexiglass stuff for energytransport around wall:
#if !ISOTHERMAL
        Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
        Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
        Scalar maxTemperatureRefPM = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperature);
        Scalar maxTemperatureChangePM = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, MaxTemperatureChange);
        Scalar temperatureInside = elemVolVars[scvf.insideScvIdx()].temperature();

        Scalar temperatureRefPM = maxTemperatureChangePM*std::sin(t*2*M_PI / 86400 - M_PI/2.0) + maxTemperatureRefPM;

        values[energyEqIdx] += plexiglassThermalConductivity
                                  * (temperatureInside - temperatureRefPM)
                                  / plexiglassThickness;
#endif

        // get constant free- flow properties:
        Scalar maxTemperatureRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
        Scalar maxTemperatureChange = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MaxTemperatureChange);
        static const Scalar massFracRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassfrac);
        static const Scalar boundaryLayerThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, ConstThickness);
        static const Scalar massTransferCoefficient = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MassTransferCoefficient);

        Scalar temperatureRef = maxTemperatureChange*std::sin(t*2*M_PI / 86400 - M_PI/2.0) + maxTemperatureRef;

        //top layer of soil
        if (globalPos[2] > this->bBoxMax()[2] - eps_)
        {
            // convert mass fraction to mole fraction
            // average molar mass of air 0.028 kg/mol
            Scalar moleFracRef = massFracRef / 0.028 * FluidSystem::molarMass(wCompIdx);

            // get porous medium values:
            Scalar moleFracInside = volVars.moleFraction(nPhaseIdx, wCompIdx);

            // calculate fluxes
            Scalar evaporationRateMole = massTransferCoefficient
                                         * volVars.diffusionCoefficient(nPhaseIdx, wCompIdx)
                                         * (moleFracInside - moleFracRef)
                                         / boundaryLayerThickness
                                         * volVars.molarDensity(nPhaseIdx);

            values[conti0EqIdx] = useMoles ? evaporationRateMole : evaporationRateMole * FluidSystem::molarMass(wCompIdx);

#if !ISOTHERMAL
            values[energyEqIdx] = 0.0; //overwrite stuff from plexiglass for upper layer, there is no plexiglass but evaporation
            values[energyEqIdx] = FluidSystem::componentEnthalpy(volVars.fluidState(), nPhaseIdx, wCompIdx)
                                   * values[conti0EqIdx];
            values[energyEqIdx] += FluidSystem::thermalConductivity(elemVolVars[scvf.insideScvIdx()].fluidState(), nPhaseIdx)
                                    * (volVars.temperature() - temperatureRef)/boundaryLayerThickness;
#endif
#if !RICHARDS
            if (volVars.pressure(nPhaseIdx) - 1e5 > 0) {
                values[contiNEqIdx] = (volVars.pressure(nPhaseIdx) - 1e5)
                                      /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                       *volVars.mobility(nPhaseIdx)
                                       *this->spatialParams().permeabilityAtPos(globalPos)
                                       *volVars.density(nPhaseIdx) * volVars.massFraction(nPhaseIdx, nCompIdx);
            }
            else {
                values[contiNEqIdx] = (volVars.pressure(nPhaseIdx) - 1e5)
                                      /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                       *volVars.mobility(nPhaseIdx)
                                       *this->spatialParams().permeabilityAtPos(globalPos)
                                       *volVars.density(nPhaseIdx) * massFracRef;
            }
#endif
        }


        return values;
    }

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the initial values for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     *
     * \param globalPos The position for which the boundary type is set
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        const Scalar pw = 1.0e5 - pcTop_ - 9.81*1000*(globalPos[dimWorld-1] - this->bBoxMax()[dimWorld-1]);
        const Scalar pn = 1.0e5 - 9.81*1*(globalPos[dimWorld-1] - this->bBoxMax()[dimWorld-1]);
        values[pressureIdx] = pw;

#if !RICHARDS
        values[switchIdx] = 1.0 - MaterialLaw::sw(this->spatialParams().materialLawParamsAtPos(globalPos), pn - pw);
#endif
#if !ISOTHERMAL
        values[temperatureIdx] = 273.15 + 10;
#endif
        values.setState(bothPhases);
        return values;

    }

    bool shouldWriteRestartFile() const
    { return false; }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    std::string name_;
    std::shared_ptr<CouplingManager> couplingManager_;
    Scalar pcTop_, pcPrecip_;
    Scalar swInit_, swPrecip_;
    Scalar tPrecipStart_, precipDuration_;
    static constexpr Scalar eps_ = 1e-8;
};

} //end namespace Dumux

#endif
