// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MixedDimension
 * \brief Reference implementation of a controller class for the Newton solver.
 *
 * Usually this controller should be sufficient.
 */
#ifndef DUMUX_ROSI_NEWTON_CONTROLLER_HH
#define DUMUX_ROSI_NEWTON_CONTROLLER_HH

#include <dumux/mixeddimension/properties.hh>
#include <dumux/mixeddimension/newtoncontroller.hh>

namespace Dumux
{
/*!
 * \ingroup NewtonController
 * \brief A reference implementation of a Newton controller specific
 *        for the coupled problems of mixed dimension
 *
 * If you want to specialize only some methods but are happy with the
 * defaults of the reference controller, derive your controller from
 * this class and simply overload the required methods.
 */
template <class TypeTag>
class RosiNewtonController : public MixedDimensionNewtonController<TypeTag>
{
    using ParentType = MixedDimensionNewtonController<TypeTag>;
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);

public:
    /*!
     * \brief Constructor
     */
    RosiNewtonController(const Problem &problem): ParentType(problem)
    { }

    /*!
     * \brief Indicates that we're done solving the non-linear system
     *        of equations.
     */
    void newtonEnd()
    {
        this->problem_().lowDimProblem().setWaterStress();
        bool switchBC = this->problem_().lowDimProblem().getSwitchBC();

        if (switchBC){
            DUNE_THROW(NumericalProblem,
                           "switch collar BC");
        }
    }

    /*!
     * \brief Called if the Newton method broke down.
     *
     * This method is called _after_ newtonEnd()
     */
    void newtonFail()
    {
        if (this->problem_().lowDimProblem().getSwitchBC())
            this->numSteps_ = this->targetSteps_;
        else
            this->numSteps_ = this->targetSteps_*2;
         this->problem_().lowDimProblem().resetSwitchBC();
    }

};

} // namespace Dumux

#endif
