// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A sub problem for the 2p2c soil problem
 */
#ifndef DUMUX_ROSI_SOIL_PROBLEM_HH
#define DUMUX_ROSI_SOIL_PROBLEM_HH

#include <cmath>

#include <dumux/implicit/cellcentered/tpfa/properties.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/porousmediumflow/richards/implicit/model.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

//! get the properties needed for subproblems
#include <dumux/mixeddimension/subproblemproperties.hh>

#include "richards_evaporationtestspatialparams.hh"

#include <dumux/material/constraintsolvers/misciblemultiphasecomposition.hh>
#include <dumux/material/fluidstates/compositional.hh>

#include <dumux/io/gnuplotinterface.hh>

namespace Dumux
{
template <class TypeTag>
class SoilTestProblem;

namespace Properties
{
#if !ISOTHERMAL
NEW_TYPE_TAG(SoilTestProblem, INHERITS_FROM(CCTpfaModel, RichardsNI, RichardsTestSpatialParams));
#else
NEW_TYPE_TAG(SoilTestProblem, INHERITS_FROM(CCTpfaModel, Richards, RichardsTestSpatialParams));
#endif

// Set the grid type
SET_TYPE_PROP(SoilTestProblem, Grid, Dune::YaspGrid<3, Dune::TensorProductCoordinates<double, 3> >);

SET_BOOL_PROP(SoilTestProblem, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(SoilTestProblem, EnableGlobalVolumeVariablesCache, true);
SET_BOOL_PROP(SoilTestProblem, EnableGlobalFluxVariablesCache, true);
SET_BOOL_PROP(SoilTestProblem, SolutionDependentAdvection, true);

// Set the problem property
SET_TYPE_PROP(SoilTestProblem, Problem, SoilTestProblem<TypeTag>);

// Set the spatial parameters
SET_TYPE_PROP(SoilTestProblem, SpatialParams, RichardsTestSpatialParams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(SoilTestProblem, ProblemEnableGravity, true);

// Enable velocity output
SET_BOOL_PROP(SoilTestProblem, VtkAddVelocity, false);

// Set the grid parameter group
SET_STRING_PROP(SoilTestProblem, GridParameterGroup, "SoilGrid");

}


/*!
 * \ingroup MixedDimension
 * \ingroup ImplicitTestProblems
 */
template <class TypeTag>
class SoilTestProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    using ParentType = ImplicitPorousMediaProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using PointSource = typename GET_PROP_TYPE(TypeTag, PointSource);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using MaterialLaw = typename GET_PROP_TYPE(TypeTag, MaterialLaw);
    using SubControlVolumeFace = typename GET_PROP_TYPE(TypeTag, SubControlVolumeFace);
#if !ISOTHERMAL
    using ThermalConductivityModel = typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel);
#endif
    using fluidsystemH20Air = typename FluidSystems::H2OAir<Scalar,Dumux::SimpleH2O<Scalar>,true>;
    using MiscibleMultiPhaseComposition = Dumux::MiscibleMultiPhaseComposition<Scalar,fluidsystemH20Air >;


    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum
    {
#if !ISOTHERMAL
        energyEqIdx = Indices::energyEqIdx
#endif

    };
    enum
    {
        // copy some indices for convenience
        pressureIdx = Indices::pressureIdx,
        conti0EqIdx = Indices::conti0EqIdx,
#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx
#endif
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using GlobalProblemTypeTag = typename GET_PROP_TYPE(TypeTag, GlobalProblemTypeTag);
    using CouplingManager = typename GET_PROP_TYPE(GlobalProblemTypeTag, CouplingManager);

public:
    SoilTestProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        noEvap_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.NoEvap);
        if (noEvap_)
            name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "-soil_noEvap";
        else
            name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "-soil";
        FluidSystem::init();
        static const Scalar sw = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InitialSoilWaterSaturationTop);
        pcTop_ = MaterialLaw::pc(this->spatialParams().materialLawParamsAtPos(this->bBoxMax()), sw);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Return the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    /*!
     * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     */
    Scalar nonWettingReferencePressure() const
    { return 1.0e5; };


    /*!
     * \brief Applies a vector of point sources. The point sources
     *        are possibly solution dependent.
     *
     * \param pointSources A vector of PointSource s that contain
              source values for all phases and space positions.
     *
     * For this method, the \a values method of the point source
     * has to return the absolute mass rate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().bulkPointSources(); }

    /*!
     * \brief Evaluate the point sources (added by addPointSources)
     *        for all phases within a given sub-control-volume.
     *
     * This is the method for the case where the point source is
     * solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param pointSource A single point source
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub-control volume within the element
     *
     * For this method, the \a values() method of the point sources returns
     * the absolute rate mass generated or annihilate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        // compute source at every integration point
        const auto& bulkVolVars = this->couplingManager().bulkVolVars(source.id());
        const auto& lowDimVolVars = this->couplingManager().lowDimVolVars(source.id());

        const auto& spatialParams = this->couplingManager().lowDimProblem().spatialParams();
        const unsigned int lowDimElementIdx = this->couplingManager().pointSourceData(source.id()).lowDimElementIdx();
        const Scalar Kr = spatialParams.Kr(lowDimElementIdx);
        const Scalar rootRadius = spatialParams.radius(lowDimElementIdx);

        PrimaryVariables sourceValues(0.0);
        // advective fluxes, convert to molar fluxes if using mole fractions
        const Scalar waterMassSink = 2 * M_PI *rootRadius * Kr *(lowDimVolVars.pressure(0) - bulkVolVars.pressure(0))
                                       * bulkVolVars.density(0);

        sourceValues[conti0EqIdx] = waterMassSink;
        sourceValues[conti0EqIdx] *= source.quadratureWeight()*source.integrationElement();
 #if !ISOTHERMAL
        //convective heat transport
        if (sourceValues[conti0EqIdx] > 0){
            sourceValues[energyEqIdx] += sourceValues[conti0EqIdx] * lowDimVolVars.enthalpy(0);
        }
        else{
            sourceValues[energyEqIdx] += sourceValues[conti0EqIdx] * bulkVolVars.enthalpy(0);
        }
        //conduction
         sourceValues[energyEqIdx] += -2* M_PI *rootRadius *0.5* (bulkVolVars.temperature() - lowDimVolVars.temperature());
#endif
        source = sourceValues;
    }

    //! Called after every time step
    //! Output the total global exchange term
    void postTimeStep()
    {
        ParentType::postTimeStep();

        PrimaryVariables source(0.0);
        Scalar evaporation = 0.0;
        Scalar waterSource = 0.0;
        if (!(this->timeManager().time() < 0.0))
        {
            for (const auto& element : elements(this->gridView()))
            {
                auto fvGeometry = localView(this->model().fvGridGeometry());
                fvGeometry.bindElement(element);

                auto elemVolVars = localView(this->model().curGlobalVolVars());
                elemVolVars.bindElement(element, fvGeometry, this->model().curSol());

                for (auto&& scv : scvs(fvGeometry))
                {
                    const auto& volVars = elemVolVars[scv];
                    auto pointSources = this->scvPointSources(element, fvGeometry, elemVolVars, scv);
                    pointSources *= scv.volume() * volVars.extrusionFactor();
                    source += pointSources;
                }

                for (auto&& scvf : scvfs(fvGeometry))
                    if (scvf.boundary())
                        evaporation += neumann(element, fvGeometry, elemVolVars, scvf)[conti0EqIdx]
                                       * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
            }
        }
        // convert to kg/s if using mole fractions
        waterSource = source[conti0EqIdx];
        evaporation = evaporation;
        std::cout << "Global integrated source (soil): " << waterSource << " kg/s / "
                  <<                           waterSource*3600*24*1000 << " g/day" << '\n';

        std::cout << "Soil evaporation rate: " << evaporation << " kg/s." << '\n';
        //do a gnuplot
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();
        x_.push_back(time); // in seconds
        y_.push_back(waterSource);
        y2_.push_back(evaporation);

        gnuplot_.resetPlot();
        gnuplot_.setXRange(0, 86400);
        gnuplot_.setYRange(0, 2e-6);
        gnuplot_.setXlabel("time [s]");
        gnuplot_.setYlabel("kg/s");
        if (noEvap_)
            gnuplot_.addDataSetToPlot(x_, y2_, "evaporation noEvap");
        else
            gnuplot_.addDataSetToPlot(x_, y2_, "evaporation");
        gnuplot_.plot("evaporation rate");

        gnuplot2_.resetPlot();
        gnuplot2_.setXRange(0, 86400);
        gnuplot2_.setYRange(0, -2e-6);
        gnuplot2_.setXlabel("time [s]");
        gnuplot2_.setYlabel("kg/s");
        if (noEvap_)
            gnuplot2_.addDataSetToPlot(x_, y_, "transpiration noEvap");
       else
           gnuplot2_.addDataSetToPlot(x_, y_, "transpiration");
       gnuplot2_.plot("transpiration rate");
    }

    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
         BoundaryTypes bcTypes;
            bcTypes.setAllNeumann();
        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    { return initialAtPos(globalPos); }

    // \}
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
     */
    PrimaryVariables neumann(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        //plexiglass stuff for energytransport around wall:
#if !ISOTHERMAL
        Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
        Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
        Scalar maxTemperatureRefPM = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperature);
        Scalar maxTemperatureChangePM = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, MaxTemperatureChange);
        Scalar temperatureInside = volVars.temperature();

        const Scalar t = this->timeManager().time() + this->timeManager().timeStepSize();
        Scalar temperatureRefPM = maxTemperatureChangePM*std::sin(t*2*M_PI / 86400 - M_PI/2.0) + maxTemperatureRefPM;

        values[energyEqIdx] += plexiglassThermalConductivity
                                  * (temperatureInside - temperatureRefPM)
                                  / plexiglassThickness;
#endif

        // get constant free- flow properties:
        Scalar maxTemperatureRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
        Scalar maxTemperatureChange = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MaxTemperatureChange);
        static const Scalar massFracRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassfrac);
        static const Scalar boundaryLayerThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, ConstThickness);
        static const Scalar massTransferCoefficient = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MassTransferCoefficient);

        Scalar temperatureRef = maxTemperatureChange*std::sin(t*2*M_PI / 86400 - M_PI/2.0) + maxTemperatureRef;

        //top layer of soil
        if (globalPos[2] > this->bBoxMax()[2] - eps_)
        {
            // convert mass fraction to mole fraction
            // average molar mass of air 0.028 kg/mol
            Scalar moleFracRef = massFracRef / 0.028 * FluidSystem::molarMass(0);

            // now we have to calculate the values for water in air when air has a constant pressure of 1 bar. We will calculate that same as in the 2p2c model with H20Air as the fluidsystem
            Dumux::CompositionalFluidState<Scalar,fluidsystemH20Air> compfluidstate;
            typename fluidsystemH20Air::ParameterCache paramCache;
            paramCache.updateAll(compfluidstate);

            compfluidstate.setPressure(0, volVars.pressure(0));
            compfluidstate.setPressure(1, 1e5);
            compfluidstate.setTemperature(volVars.temperature());

            MiscibleMultiPhaseComposition::solve(compfluidstate,
                                                 paramCache,
                                                 /*setViscosity=*/true,
                                                 /*setEnthalpy=*/false);
            Scalar moleFracInside = compfluidstate.moleFraction(1,0);
            // binary diffusion coefficients of water in air
            Scalar diffCoeff =  fluidsystemH20Air::binaryDiffusionCoefficient(compfluidstate, 1,0,1);
            Scalar evaporationRateMole = massTransferCoefficient
                                         * diffCoeff
                                         * (moleFracInside - moleFracRef)
                                         / boundaryLayerThickness
                                         * compfluidstate.molarDensity(1);


            values[conti0EqIdx] = evaporationRateMole * fluidsystemH20Air::molarMass(0);
            if (noEvap_)
                values[conti0EqIdx] /= 1e8;
#if !ISOTHERMAL
            values[energyEqIdx] = 0.0; //overwrite stuff from plexiglass for upper layer, there is no plexiglass but evaporation
            values[energyEqIdx] = fluidsystemH20Air::componentEnthalpy(compfluidstate, 1, 0)
                                   * values[conti0EqIdx];
            values[energyEqIdx] += fluidsystemH20Air::thermalConductivity(compfluidstate, 1)
                                    * (volVars.temperature() - temperatureRef)/boundaryLayerThickness;
#endif
        }

        return values;
    }

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the initial values for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     *
     * \param globalPos The position for which the boundary type is set
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        const Scalar pw = 1.0e5 - pcTop_ - 9.81*1000*(globalPos[dimWorld-1] - this->bBoxMax()[dimWorld-1]);
        values[pressureIdx] = pw;

#if !ISOTHERMAL
        values[temperatureIdx] = 273.15 + 10;
#endif
        values.setState(Indices::bothPhases);
        return values;

    }

    bool shouldWriteRestartFile() const
    { return false; }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    std::string name_;
    std::shared_ptr<CouplingManager> couplingManager_;
    Scalar pcTop_;
    static constexpr Scalar eps_ = 1e-8;

    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;

    std::vector<double> x_;
    std::vector<double> y_;
    std::vector<double> y2_;

    bool noEvap_;
};

} //end namespace Dumux

#endif
