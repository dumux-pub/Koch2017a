// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem the rootsystem coupled with 2p2c in the bulk
 */
#ifndef DUMUX_ROSI_EVAPORATION_PROBLEM_HH
#define DUMUX_ROSI_EVAPORATION_PROBLEM_HH

#define ISOTHERMAL 0

#include "rootsystemtestproblem.hh"
#include "2p_evaporationtestproblem.hh"

#include <dumux/mixeddimension/problem.hh>
#include <dumux/mixeddimension/embedded/cellcentered/bboxtreecouplingmanager.hh>
#include <dumux/mixeddimension/integrationpointsource.hh>

namespace Dumux
{
template <class TypeTag>
class RosiEvaporationProblem;

namespace Properties
{
NEW_TYPE_TAG(RosiEvaporationProblem, INHERITS_FROM(MixedDimension));

// Set the problem property
SET_TYPE_PROP(RosiEvaporationProblem, Problem, RosiEvaporationProblem<TypeTag>);

// Set the coupling manager
SET_TYPE_PROP(RosiEvaporationProblem, CouplingManager, CCBBoxTreeEmbeddedCouplingManager<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(RosiEvaporationProblem, LowDimProblemTypeTag, TTAG(RootsystemTestProblem));
SET_TYPE_PROP(RosiEvaporationProblem, BulkProblemTypeTag, TTAG(SoilTestProblem));

// publish this problem in the sub problems
SET_TYPE_PROP(RootsystemTestProblem, GlobalProblemTypeTag, TTAG(RosiEvaporationProblem));
SET_TYPE_PROP(SoilTestProblem, GlobalProblemTypeTag, TTAG(RosiEvaporationProblem));

// The subproblems inherit the parameter tree from this problem
SET_PROP(RootsystemTestProblem, ParameterTree) : GET_PROP(TTAG(RosiEvaporationProblem), ParameterTree) {};
SET_PROP(SoilTestProblem, ParameterTree) : GET_PROP(TTAG(RosiEvaporationProblem), ParameterTree) {};

// Set the point source type of the subproblems to an integration point source
SET_TYPE_PROP(RootsystemTestProblem, PointSource, Dumux::IntegrationPointSource<TTAG(RootsystemTestProblem), unsigned int>);
SET_TYPE_PROP(RootsystemTestProblem, PointSourceHelper, Dumux::IntegrationPointSourceHelper<TTAG(RootsystemTestProblem)>);
SET_TYPE_PROP(SoilTestProblem, PointSource, Dumux::IntegrationPointSource<TTAG(SoilTestProblem), unsigned int>);
SET_TYPE_PROP(SoilTestProblem, PointSourceHelper, Dumux::IntegrationPointSourceHelper<TTAG(SoilTestProblem)>);

//SET_TYPE_PROP(RosiEvaporationProblem, LinearSolver, ILU0BiCGSTABBackend<TypeTag>);
SET_TYPE_PROP(RosiEvaporationProblem, LinearSolver, UMFPackBackend<TypeTag>);

}//end namespace properties

template <class TypeTag>
class RosiEvaporationProblem : public MixedDimensionProblem<TypeTag>
{
    using ParentType = MixedDimensionProblem<TypeTag>;
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    // obtain the type tags of the sub problems
    using BulkProblemTypeTag = typename GET_PROP_TYPE(TypeTag, BulkProblemTypeTag);
    using LowDimProblemTypeTag = typename GET_PROP_TYPE(TypeTag, LowDimProblemTypeTag);

    // obtain types from the sub problem type tags
    using BulkProblem = typename GET_PROP_TYPE(BulkProblemTypeTag, Problem);
    using LowDimProblem = typename GET_PROP_TYPE(LowDimProblemTypeTag, Problem);

    using BulkGridView = typename GET_PROP_TYPE(BulkProblemTypeTag, GridView);
    using LowDimGridView = typename GET_PROP_TYPE(LowDimProblemTypeTag, GridView);

public:
    RosiEvaporationProblem(TimeManager &timeManager, const BulkGridView &bulkGridView, const LowDimGridView &lowDimgridView)
    : ParentType(timeManager, bulkGridView, lowDimgridView)
    {}

    bool shouldWriteOutput() const
    { return true; }
};

} // end namespace Dumux

#endif
