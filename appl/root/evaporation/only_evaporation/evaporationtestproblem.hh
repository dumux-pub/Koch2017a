// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A 2p2c problem including evaporation as a boundary condition
 */
#ifndef DUMUX_SOIL_EVAPORATION_PROBLEM_HH
#define DUMUX_SOIL_EVAPORATION_PROBLEM_HH

#include <cmath>

#include <dumux/implicit/cellcentered/tpfa/properties.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include "evaporationtestspatialparams.hh"

#include <dumux/io/gnuplotinterface.hh>

namespace Dumux
{
template <class TypeTag>
class SoilTestProblem;

namespace Properties
{
#if !ISOTHERMAL
NEW_TYPE_TAG(SoilTestProblem, INHERITS_FROM(CCTpfaModel, TwoPTwoCNI, TwoPTwoCTestSpatialParams));
#else
NEW_TYPE_TAG(SoilTestProblem, INHERITS_FROM(CCTpfaModel, TwoPTwoC, TwoPTwoCTestSpatialParams));
#endif

// Set the grid type
SET_TYPE_PROP(SoilTestProblem, Grid, Dune::YaspGrid<3, Dune::TensorProductCoordinates<double, 3> >);

SET_BOOL_PROP(SoilTestProblem, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(SoilTestProblem, EnableGlobalVolumeVariablesCache, true);
SET_BOOL_PROP(SoilTestProblem, EnableGlobalFluxVariablesCache, true);
SET_BOOL_PROP(SoilTestProblem, SolutionDependentAdvection, true);

// Set the problem property
SET_TYPE_PROP(SoilTestProblem, Problem, SoilTestProblem<TypeTag>);

// Set the spatial parameters
SET_TYPE_PROP(SoilTestProblem, SpatialParams, TwoPTwoCTestSpatialParams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(SoilTestProblem, ProblemEnableGravity, true);

// Enable velocity output
SET_BOOL_PROP(SoilTestProblem, VtkAddVelocity, true);

// Set the grid parameter group
SET_STRING_PROP(SoilTestProblem, GridParameterGroup, "SoilGrid");

// Choose pn and Sw as primary variables
SET_INT_PROP(SoilTestProblem, Formulation, TwoPTwoCFormulation::pwsn);

// Choose the h2o air fluid system with simple h2o water type
SET_TYPE_PROP(SoilTestProblem,
              FluidSystem,
              Dumux::FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar),
                                          Dumux::SimpleH2O<typename GET_PROP_TYPE(TypeTag, Scalar)>,
                                          /*useComplexrelations=*/false>);

// Use mass fractions
SET_BOOL_PROP(SoilTestProblem, UseMoles, false);

//! Determines whether the constraint solver is used
SET_BOOL_PROP(SoilTestProblem, UseConstraintSolver, true);

//Kelvin Equation for the calculation of the vapor pressure is used
SET_BOOL_PROP(SoilTestProblem, UseKelvinEquation, true);
}

/*!
 * \ingroup 2p2c
 * \ingroup ImplicitTestProblems
 */
template <class TypeTag>
class SoilTestProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    using ParentType = ImplicitPorousMediaProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NeumannFluxes = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using PointSource = typename GET_PROP_TYPE(TypeTag, PointSource);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using MaterialLaw = typename GET_PROP_TYPE(TypeTag, MaterialLaw);
    using SubControlVolumeFace = typename GET_PROP_TYPE(TypeTag, SubControlVolumeFace);
#if !ISOTHERMAL
    using ThermalConductivityModel = typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel);
#endif

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum
    {
        conti0EqIdx = Indices::conti0EqIdx,
        contiWEqIdx = Indices::contiWEqIdx,
        contiNEqIdx = Indices::contiNEqIdx,
#if !ISOTHERMAL
        energyEqIdx = Indices::energyEqIdx
#endif

    };
    enum
    {
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx
#endif
    };
   enum
   {
       // the indices for the phase presence
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases
    };
    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    SoilTestProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        noEvap_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.NoEvap);
        if (noEvap_)
            name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "-soil_noEvap";
        else
            name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "-soil";
        FluidSystem::init();
        sw_initial_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InitialSoilWaterSaturation);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Return the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 15C


   //! Called after every time step
    //! Output the total evaporation rate
    void postTimeStep()
    {
        ParentType::postTimeStep();

        PrimaryVariables source(0.0);
        Scalar evaporation = 0.0;
        if (!(this->timeManager().time() < 0.0))
        {
            for (const auto& element : elements(this->gridView()))
            {
                auto fvGeometry = localView(this->model().fvGridGeometry());
                fvGeometry.bindElement(element);

                auto elemVolVars = localView(this->model().curGlobalVolVars());
                elemVolVars.bindElement(element, fvGeometry, this->model().curSol());

                for (auto&& scvf : scvfs(fvGeometry))
                    if (scvf.boundary())
                        evaporation += neumann(element, fvGeometry, elemVolVars, scvf)[contiWEqIdx]
                                       * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
            }
        }
        // convert to kg/s if using mole fractions
        evaporation = useMoles ? evaporation * FluidSystem::molarMass(wCompIdx) : evaporation;
        std::cout << "Soil evaporation rate: " << evaporation << " kg/s." << '\n';
        //do a gnuplot
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();
        x_.push_back(time); // in seconds
        y_.push_back(evaporation);

        gnuplot_.resetPlot();
        gnuplot_.setXRange(0,std::max(time, 86400.0));
        gnuplot_.setYRange(0, 2e-6);
        gnuplot_.setXlabel("time [s]");
        gnuplot_.setYlabel("kg/s");
        if (noEvap_)
            gnuplot_.addDataSetToPlot(x_, y_, "evaporation noEvap");
        else
            gnuplot_.addDataSetToPlot(x_, y_, "evaporation");
        gnuplot_.plot("evaporation rate");

    }

    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
         BoundaryTypes bcTypes;
            bcTypes.setAllNeumann();
        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    { return initialAtPos(globalPos); }

    // \}
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
     */
    NeumannFluxes neumann(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        //plexiglass stuff for energytransport around wall:
#if !ISOTHERMAL
        Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
        Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
        Scalar maxTemperatureRefPM = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperature);
        Scalar maxTemperatureChangePM = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, MaxTemperatureChange);
        Scalar temperatureInside = elemVolVars[scvf.insideScvIdx()].temperature();

        const Scalar t = this->timeManager().time() + this->timeManager().timeStepSize();
        Scalar temperatureRefPM = maxTemperatureChangePM*std::sin(t*2*M_PI /86400 - M_PI/2.0) + maxTemperatureRefPM;

        values[energyEqIdx] += plexiglassThermalConductivity
                                  * (temperatureInside - temperatureRefPM)
                                  / plexiglassThickness;
#endif

        // get constant free- flow properties:
        Scalar maxTemperatureRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
        Scalar maxTemperatureChange = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MaxTemperatureChange);
        static const Scalar massFracRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassfrac);
        static const Scalar boundaryLayerThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, ConstThickness);
        static const Scalar massTransferCoefficient = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MassTransferCoefficient);

        Scalar temperatureRef = maxTemperatureChange*std::sin(t*2*M_PI / 86400 - M_PI/2.0) + maxTemperatureRef;

        //top layer of soil
        if (globalPos[2] > this->bBoxMax()[2] - eps_)
        {
            // convert mass fraction to mole fraction
            // average molar mass of air 0.028 kg/mol
            Scalar moleFracRef = massFracRef / 0.028 * FluidSystem::molarMass(wCompIdx);

            // get porous medium values:
            Scalar moleFracInside = volVars.moleFraction(nPhaseIdx, wCompIdx);

            // calculate fluxes
            Scalar evaporationRateMole = massTransferCoefficient
                                         * volVars.diffusionCoefficient(nPhaseIdx, wCompIdx)
                                         * (moleFracInside - moleFracRef)
                                         / boundaryLayerThickness
                                         * volVars.molarDensity(nPhaseIdx);

            values[contiWEqIdx] = useMoles ? evaporationRateMole : evaporationRateMole * FluidSystem::molarMass(wCompIdx);
            if (noEvap_)
                values[contiWEqIdx] /= 1e8;

            if (volVars.pressure(nPhaseIdx) - 1e5 > 0) {
                values[contiNEqIdx] = (volVars.pressure(nPhaseIdx) - 1e5)
                                      /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                       *volVars.mobility(nPhaseIdx)
                                       *this->spatialParams().permeabilityAtPos(globalPos)
                                       *volVars.density(nPhaseIdx) * volVars.massFraction(nPhaseIdx, nCompIdx);
             }
             else {
                values[contiNEqIdx] = (volVars.pressure(nPhaseIdx) - 1e5)
                                      /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                       *volVars.mobility(nPhaseIdx)
                                       *this->spatialParams().permeabilityAtPos(globalPos)
                                       *volVars.density(nPhaseIdx) * massFracRef;
             }
#if !ISOTHERMAL
            //overwrite stuff from plexiglass
            values[energyEqIdx] = 0.0;
            values[energyEqIdx] = FluidSystem::componentEnthalpy(volVars.fluidState(), nPhaseIdx, wCompIdx) * values[contiWEqIdx];
            values[energyEqIdx] += FluidSystem::componentEnthalpy(volVars.fluidState(), nPhaseIdx, nCompIdx) * values[contiNEqIdx];
            values[energyEqIdx] += FluidSystem::thermalConductivity(elemVolVars[scvf.insideScvIdx()].fluidState(), nPhaseIdx) * (volVars.temperature() - temperatureRef)/boundaryLayerThickness;
#endif
        }
        return values;
    }

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the initial values for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     *
     * \param globalPos The position for which the boundary type is set
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        const Scalar sw = sw_initial_;
        const Scalar pc = MaterialLaw::pc(this->spatialParams().materialLawParamsAtPos(globalPos), sw);
        values[pressureIdx] = 1e5 - pc;
        values[switchIdx] = 1- sw_initial_;
#if !ISOTHERMAL
        values[temperatureIdx] = 273.15 + 12;
#endif
        values.setState(bothPhases);
        return values;

    }


    bool shouldWriteRestartFile() const
    { return false; }

private:
    std::string name_;
    static constexpr Scalar eps_ = 1e-8;

    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;

    std::vector<double> x_;
    std::vector<double> y_;
    Scalar sw_initial_;

    bool noEvap_;
};

} //end namespace Dumux

#endif
