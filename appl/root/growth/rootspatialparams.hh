// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p box model
 */
#ifndef DUMUX_ROOT_SPATIALPARAMS_GROWTH_HH
#define DUMUX_ROOT_SPATIALPARAMS_GROWTH_HH

#include <dumux/growth/spatialparams.hh>
#include <dumux/material/components/simpleh2o.hh>

namespace Dumux
{

/*!
 * \ingroup GridGrowth
 *
 * \brief The spatial parameters for root growth problem
 */
template<class TypeTag>
class RootSpatialParams : public RootGrowthSpatialParams<TypeTag>
{
    using ParentType = RootGrowthSpatialParams<TypeTag>;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using ElementSolutionVector = typename GET_PROP_TYPE(TypeTag, ElementSolutionVector);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    using GlobalPosition = Dune::FieldVector<Scalar,dimWorld>;
    using Element = typename GridView::template Codim<0>::Entity;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    RootSpatialParams(const Problem& problem, const GridView& gridView)
        : ParentType(problem, gridView)
    {}

    void init()
    {
        ParentType::init();
        // setup the branches
        this->createBranchParams("0", 0, 1, GlobalPosition({-0.03, 0.0, -0.0015}), GlobalPosition({0, 0, -1}));
        this->createBranchParams("0", 0, 3, GlobalPosition({0.03, 0.0, -0.0015}),  GlobalPosition({0, 0, -1}));
        this->branchParams(0).length = std::abs(this->problem().bBoxMax()[2]-this->problem().bBoxMin()[2]);
        this->branchParams(0).orientation = {0, 0, -1};
        this->branchParams(1).length = std::abs(this->problem().bBoxMax()[2]-this->problem().bBoxMin()[2]);
        this->branchParams(1).orientation = {0, 0, -1};
    }

    /*!
     * \brief Return the intrinsic permeability for the current sub-control volume in [m^2].
     *
     * \param ipGlobal The integration point
     * \note Kx has units [m^4/(Pa*s)] so we have to divide by the cross-section area
     *       and multiply with a characteristic viscosity
     */
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolutionVector& elemSol) const
    {
        const Scalar r = this->rootParams(element).radius;
        return this->rootParams(element).axialPerm / (M_PI*r*r)
               * SimpleH2O<Scalar>::liquidViscosity(295.15, elemSol[0][Indices::pressureIdx]);
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The element
     * \param scv The sub control volume
     * \param elemSol The element solution vector
     * \return the porosity
     */
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolutionVector& elemSol) const
    { return 0.4; }

};

} // end namespace Dumux

#endif
