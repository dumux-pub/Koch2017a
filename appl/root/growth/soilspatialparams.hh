// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup GridGrowth
 * \brief Spatial parameters for growth test
 */
#ifndef DUMUX_SOIL_SPATIALPARAMS_GROWTH_HH
#define DUMUX_SOIL_SPATIALPARAMS_GROWTH_HH

#include <dumux/material/spatialparams/implicit.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{

// forward declaration
template<class TypeTag>
class SoilSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(SoilSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(SoilSpatialParams, SpatialParams, SoilSpatialParams<TypeTag>);

// Set the material law
SET_PROP(SoilSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    // define the material law parameterized by absolute saturations
    using type = EffToAbsLaw<VanGenuchten<Scalar>>;
};
}

/*!
 * \ingroup GridGrowth
 * \brief The spatial parameters for the root growth soil
 */
template<class TypeTag>
class SoilSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    using ParentType = ImplicitSpatialParams<TypeTag>;
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using MaterialLaw = typename GET_PROP_TYPE(TypeTag, MaterialLaw);
    using MaterialLawParams = typename MaterialLaw::Params;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    /*!
     * \brief Constructor
     *
     * \param gridView The DUNE GridView representing the spatial
     *                 domain of the problem.
     */
    SoilSpatialParams(const Problem& problem, const GridView& gridView)
        : ParentType(problem, gridView)
    {
       // residual saturations
        materialParams_.setSwr(0.05);
        materialParams_.setSnr(0.0);

        // parameters for the Van Genuchten law
        // alpha and n
        materialParams_.setVgAlpha(2.956e-4);
        materialParams_.setVgn(1.5);

        permeability_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Permeability);
    }

    /*!
     * \brief Returns the intrinsic permeability tensor [m^2] at a given location
     *
     * \param globalPos The global position where we evaluate
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    /*!
     * \brief Returns the porosity [] at a given location
     *
     * \param globalPos The global position where we evaluate
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 0.4; }

    /*!
     * \brief Returns the parameters for the material law at a given location
     *
     * This method is not actually required by the Richards model, but provided
     * for the convenience of the RichardsLensProblem
     *
     * \param globalPos A global coordinate vector
     */
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition &globalPos) const
    { return materialParams_; }

//     Scalar porosity(const Element &element,
//                     const Problem & problem,
//                     const FVElementGeometry &fvGeometry,
//                     const int scvIdx) const
//      {
//             Scalar poro(0.0);
//             auto lowDimVolFrac = problem.couplingManager().lowDimVolumeFraction(element)*0.6;
// //             if (lowDimVol > 0){
// //             int eIdx = problem.model().elementMapper().index(element);
// //             std::cout<<"lowDimVol"<<lowDimVol<<std::endl;
// //             std::cout<<"bulkvol"<<fvGeometry.subContVol[scvIdx].volume<<std::endl;
// //             std::cout<<"initialPorosity"<<asImp_().initialPorosity(element, fvGeometry, scvIdx)<<std::endl;
// //             std::cout<<"eidx"<<eIdx<<std::endl;
// //             }
//             poro += asImp_().initialPorosity(element, fvGeometry, scvIdx) - lowDimVolFrac;
//             return poro;
// //          return 0.6;
//     }

private:
    MaterialLawParams materialParams_;
    Scalar permeability_;
};

} // end namespace Dumux

#endif
