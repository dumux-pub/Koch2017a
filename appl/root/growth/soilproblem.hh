// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A sub problem for the richards problem with a growing root grid
 */
#ifndef DUMUX_SOIL_PROBLEM_GROWTH_HH
#define DUMUX_SOIL_PROBLEM_GROWTH_HH

#include <dumux/implicit/cellcentered/tpfa/properties.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/porousmediumflow/richards/implicit/model.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>

//! get the properties needed for subproblems
#include <dumux/mixeddimension/subproblemproperties.hh>

#include "soilspatialparams.hh"

namespace Dumux
{
template <class TypeTag>
class SoilProblem;

namespace Properties
{
NEW_TYPE_TAG(SoilProblem, INHERITS_FROM(CCTpfaModel, Richards, GridGrowth, SoilSpatialParams));

// Set the wetting phase
SET_PROP(SoilProblem, WettingPhase)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::LiquidPhase<Scalar, SimpleH2O<Scalar>>;
};

// Set the grid type
// SET_TYPE_PROP(SoilProblem, Grid, Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3> >);
SET_TYPE_PROP(SoilProblem, Grid, Dune::UGGrid<3>);

//! enable caching for speedup
SET_BOOL_PROP(SoilProblem, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(SoilProblem, EnableGlobalVolumeVariablesCache, true);
SET_BOOL_PROP(SoilProblem, EnableGlobalFluxVariablesCache, true);
SET_BOOL_PROP(SoilProblem, SolutionDependentAdvection, false);

// Set the problem property
SET_TYPE_PROP(SoilProblem, Problem, SoilProblem<TypeTag>);

// Set the spatial parameters
SET_TYPE_PROP(SoilProblem, SpatialParams, SoilSpatialParams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(SoilProblem, ProblemEnableGravity, false);

// Enable velocity output
SET_BOOL_PROP(SoilProblem, VtkAddVelocity, true);

// Set the grid parameter group
SET_STRING_PROP(SoilProblem, GridParameterGroup, "SoilGrid");

}

/*!
 * \ingroup GridGrowth
 * \brief Plant root growth
 */
template <class TypeTag>
class SoilProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    using ParentType = ImplicitPorousMediaProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using PointSource = typename GET_PROP_TYPE(TypeTag, PointSource);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum { conti0EqIdx = Indices::conti0EqIdx };
    enum { pressureIdx = Indices::pressureIdx };
    enum { wPhaseIdx = Indices::wPhaseIdx };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using GlobalProblemTypeTag = typename GET_PROP_TYPE(TypeTag, GlobalProblemTypeTag);
    using CouplingManager = typename GET_PROP_TYPE(GlobalProblemTypeTag, CouplingManager);

public:
    SoilProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "-soil";
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Called by the time manager after the time integration.
     */
    void preTimeStep()
    {
        ParentType::preTimeStep();

        // if the grid changed recompute the source map and the bounding box tree
        if (this->timeManager().timeStepIndex() > 0)
            if (this->couplingManager().lowDimProblem().gridGrowth().hasGrown())
               this->computePointSourceMap_();
    }

    /*!
     * \brief Return the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    /*
     * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     */
    Scalar nonWettingReferencePressure() const
    { return 1.0e5; }

    /*!
     * \brief Applies a vector of point sources. The point sources
     *        are possibly solution dependent.
     *
     * \param pointSources A vector of Dumux::PointSource s that contain
              source values for all phases and space positions.
     *
     * For this method, the \a values method of the point source
     * has to return the absolute mass rate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().bulkPointSources(); }

    /*!
     * \brief Evaluate the point sources (added by addPointSources)
     *        for all phases within a given sub-control-volume.
     *
     * This is the method for the case where the point source is
     * solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param pointSource A single point source
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub-control volume within the element
     *
     * For this method, the \a values() method of the point sources returns
     * the absolute rate mass generated or annihilate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        // compute source at every integration point
        const auto& bulkPriVars = this->couplingManager().bulkPriVars(source.id());
        const auto& lowDimVolVars = this->couplingManager().lowDimVolVars(source.id());

        const auto& spatialParams = this->couplingManager().lowDimProblem().spatialParams();
        const unsigned int lowDimElementIdx = this->couplingManager().pointSourceData(source.id()).lowDimElementIdx();
        const Scalar Kr = spatialParams.rootParams(lowDimElementIdx).radialPerm;
        const Scalar rootRadius = spatialParams.rootParams(lowDimElementIdx).radius;

        // sink defined as radial flow Jr * density [m^2 s-1]* [kg m-3]
        const Scalar sourceValue = 2* M_PI *rootRadius * Kr *(lowDimVolVars.pressure() - bulkPriVars[0])
                                   *1000;
        source = sourceValue*source.quadratureWeight()*source.integrationElement();
    }

    //! Called after every time step
    //! Output the total global exchange term
    void postTimeStep()
    {
        ParentType::postTimeStep();

        PrimaryVariables source(0.0);

        if (!(this->timeManager().time() < 0.0))
        {
            for (const auto& element : elements(this->gridView()))
            {
                auto fvGeometry = localView(this->model().fvGridGeometry());
                fvGeometry.bindElement(element);

                auto elemVolVars = localView(this->model().curGlobalVolVars());
                elemVolVars.bindElement(element, fvGeometry, this->model().curSol());

                for (auto&& scv : scvs(fvGeometry))
                {
                    auto pointSources = this->scvPointSources(element, fvGeometry, elemVolVars, scv);
                    pointSources *= scv.volume()*elemVolVars[scv].extrusionFactor();
                    source += pointSources;
                }
            }
        }

        std::cout << "Global integrated source (soil): " << source << " (kg/s) / "
                  <<                           source*3600*24*1000 << " (g/day)" << '\n';
    }

    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;
        bcTypes.setAllNeumann();
        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    { return initialAtPos(globalPos); }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial values for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     *
     * \param globalPos The position for which the boundary type is set
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        values[pressureIdx] = GET_RUNTIME_PARAM(TypeTag,
                                                Scalar,
                                                BoundaryConditions.InitialSoilPressure);
        values.setState(Indices::bothPhases);
        return values;

    }

    bool shouldWriteRestartFile() const
    { return false; }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    std::string name_;
    std::shared_ptr<CouplingManager> couplingManager_;
    const Scalar eps_ = 1e-6;
};

} // end namespace Dumux

#endif
