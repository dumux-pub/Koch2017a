// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A sub problem for the rootsystem
 */
#ifndef DUMUX_ROOT_PROBLEM_GROWTH_HH
#define DUMUX_ROOT_PROBLEM_GROWTH_HH

#include <dumux/io/container.hh>
#include <dumux/implicit/cellcentered/tpfa/properties.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/porousmediumflow/1p/implicit/model.hh>
#include <dumux/growth/properties.hh>
#include <dumux/growth/problem.hh>
#include <dumux/growth/localresidual.hh>
#include <dumux/growth/model.hh>
#include <dumux/growth/rootbox/growthalgorithm.hh>
#include <dumux/growth/rootbox/datatransferhelper.hh>
#include <dumux/linear/seqsolverbackend.hh>
// fluidsystem
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>
// spatial params
#include "rootspatialparams.hh"
//! this is a subproblem of a coupled problem
//! get the properties needed for subproblems
#include <dumux/mixeddimension/subproblemproperties.hh>

namespace Dumux
{
template <class TypeTag>
class RootProblem;

namespace Properties
{
NEW_TYPE_TAG(RootProblem, INHERITS_FROM(CCTpfaModel, OneP, GridGrowth));

SET_PROP(RootProblem, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::LiquidPhase<Scalar, SimpleH2O<Scalar> >;
};

//! enable caching for speedup
SET_BOOL_PROP(RootProblem, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(RootProblem, EnableGlobalVolumeVariablesCache, true);
SET_BOOL_PROP(RootProblem, EnableGlobalFluxVariablesCache, true);
SET_BOOL_PROP(RootProblem, SolutionDependentAdvection, false);

// Set the grid type
SET_TYPE_PROP(RootProblem, Grid, Dune::FoamGrid<1, 3>);

// Set the problem property
SET_TYPE_PROP(RootProblem, Problem, RootProblem<TypeTag>);

//! Set the default helper class for data transfer
SET_TYPE_PROP(RootProblem, BaseModel, GrowthModel<TypeTag>);

//! Set the default helper class for data transfer
SET_TYPE_PROP(RootProblem, BaseLocalResidual, GrowthLocalResidual<TypeTag>);

// Set the spatial parameters
SET_TYPE_PROP(RootProblem, SpatialParams, RootSpatialParams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(RootProblem, ProblemEnableGravity, false);

// Enable velocity output
SET_BOOL_PROP(RootProblem, VtkAddVelocity, true);

// growth properties
SET_BOOL_PROP(RootProblem, GrowingGrid, true);

//! Set the indicator class models for growth
SET_TYPE_PROP(RootProblem, GrowthAlgorithm, RootBoxGrowthAlgorithm<TypeTag>);

//! Set the default indicator class models for growth
SET_TYPE_PROP(RootProblem, GrowthDataTransferHelper, RootBoxDataTransferHelper<TypeTag>);
}

/*!
 * \ingroup GridGrowth
 * \brief Plant root growth
 */
template <class TypeTag>
class RootProblem : public GrowthProblem<TypeTag>
{
    using ParentType = GrowthProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using SubControlVolumeFace = typename GET_PROP_TYPE(TypeTag, SubControlVolumeFace);
    using ElementSolutionVector = typename GET_PROP_TYPE(TypeTag, ElementSolutionVector);
    using VtkOutputModule = typename GET_PROP_TYPE(TypeTag, VtkOutputModule);
    using PointSource = typename GET_PROP_TYPE(TypeTag, PointSource);
    using Element = typename GridView::template Codim<0>::Entity;
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum {
        // indices of the primary variables
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx
    };

    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using GlobalProblemTypeTag = typename GET_PROP_TYPE(TypeTag, GlobalProblemTypeTag);
    using CouplingManager = typename GET_PROP_TYPE(GlobalProblemTypeTag, CouplingManager);

public:
    RootProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name) + "-root";
    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     *
     * If you overload this method don't forget to call
     * ParentType::init()
     */
    void init()
    {
        ParentType::init();
        // compute root volume
        rootVolume_ = 0.0;
        for (const auto& element : elements(this->gridView()))
        {
            for (int plantId = 0; plantId < 2; ++plantId)
            {
                if (plantId == this->spatialParams().rootParams(element).plantId)
                {
                    const auto radius = this->spatialParams().rootParams(element).radius;
                    rootVolume_[plantId] += element.geometry().volume() * M_PI*radius*radius;
                }
            }
        }
    }

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     *
     * The extrusion factor here makes extrudes the 1d line to a circular tube with
     * cross-section area pi*r^2.
     */
    Scalar extrusionFactor(const Element &element,
                           const SubControlVolume &scv,
                           const ElementSolutionVector& elemSol) const
    {
        const auto radius = this->spatialParams().rootParams(element).radius;
        return M_PI*radius*radius;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief If a new point resulting from growth in inside the domain
     * \note This is used to implement confining geometry
     * (e.g. plant growth inside a container, or around a cylinder)
     */
    bool pointInDomain(const GlobalPosition& p) const
    {
        if (p[2] < -0.1 || p[2] > eps_)
            return false;

        const auto r = std::sqrt(p[0]*p[0]+p[1]*p[1]);
        const auto rContainer = 0.05 + p[2]/0.1*0.03;
        if (r > rContainer)
            return false;

        return true;
    }

    /*!
     * \brief Return the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C


    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    BoundaryTypes boundaryTypesAtPos (const GlobalPosition &globalPos ) const
    {
        BoundaryTypes bcTypes;
        bcTypes.setAllNeumann();
        return bcTypes;
    }


    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */

    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
        if (globalPos[2] + eps_ >  this->bBoxMax()[2] )
              values[pressureIdx] = GET_RUNTIME_PARAM(TypeTag,
                                                      Scalar,
                                                      BoundaryConditions.CriticalCollarPressure);
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    PrimaryVariables neumann(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        // root collar
        if ( globalPos[2] > this->bBoxMax()[2] - eps_)
        {
            // compute transpiration rate according the current root volume
            static const Scalar rootDensity = 750.0; // kg/m³
            static const Scalar rootShootRatio = 0.5; // kg/kg
            static const Scalar leafAreaPerShootMass = 20.0; // m²/kg
            static const Scalar tRatePerLeafArea = GET_RUNTIME_PARAM(TypeTag, Scalar, BoundaryConditions.TranspirationRatePerLeafArea); // m³/(s m²)
            static const Scalar h2oDensity = 1000; // kg/m³

            const int plantId = this->spatialParams().rootParams(element).plantId;

            values[conti0EqIdx] = rootVolume_[plantId]*rootDensity / rootShootRatio
                                  *leafAreaPerShootMass*tRatePerLeafArea*h2oDensity; // kg/m³
        }

        values /= volVars.extrusionFactor() * scvf.area();
        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
        values[pressureIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, BoundaryConditions.InitialRootPressure);
        return values;
    }

    /*!
     * \brief Applies a vector of point sources. The point sources
     *        are possibly solution dependent.
     *
     * \param pointSources A vector of Dumux::PointSource s that contain
              source values for all phases and space positions.
     *
     * For this method, the \a values method of the point source
     * has to return the absolute mass rate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().lowDimPointSources(); }

    //! Update the point sources after the grid has grown
    void updatePointSources()
    { couplingManager().update(); }

    /*!
     * \brief Evaluate the point sources (added by addPointSources)
     *        for all phases within a given sub-control-volume.
     *
     * This is the method for the case where the point source is
     * solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param pointSource A single point source
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub-control volume within the element
     *
     * For this method, the \a values() method of the point sources returns
     * the absolute rate mass generated or annihilate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        // compute source at every integration point
        const auto& bulkPriVars = this->couplingManager().bulkPriVars(source.id());
        const auto& lowDimVolVars = this->couplingManager().lowDimVolVars(source.id());

        const unsigned int lowDimElementIdx = this->couplingManager().pointSourceData(source.id()).lowDimElementIdx();
        const Scalar Kr = this->spatialParams().rootParams(lowDimElementIdx).radialPerm;
        const Scalar rootRadius = this->spatialParams().rootParams(lowDimElementIdx).radius;

        // sink defined as radial flow Jr * density [m^2 s-1]* [kg m-3]
        const Scalar sourceValue = 2* M_PI *rootRadius * Kr *(bulkPriVars[0] - lowDimVolVars.pressure())
                                   *1000;
        source = sourceValue*source.quadratureWeight()*source.integrationElement();
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void addVtkOutputFields(VtkOutputModule& outputModule) const
    {
        auto& radius = outputModule.createScalarField("radius", 0);
        auto& axialPerm = outputModule.createScalarField("axialPerm", 0);
        auto& radialPerm = outputModule.createScalarField("radialPerm", 0);
        auto& order = outputModule.createScalarField("order", 0);
        auto& branchId = outputModule.createScalarField("branchId", 0);
        auto& plantId = outputModule.createScalarField("plantId", 0);

        for (const auto& element : elements(this->gridView()))
        {
            const auto eIdx = this->elementMapper().index(element);
            radius[eIdx] = this->spatialParams().rootParams(eIdx).radius;
            axialPerm[eIdx] = this->spatialParams().rootParams(eIdx).axialPerm;
            radialPerm[eIdx] = this->spatialParams().rootParams(eIdx).radialPerm;
            order[eIdx] = this->spatialParams().rootParams(eIdx).order;
            branchId[eIdx] = this->spatialParams().rootParams(eIdx).branchId;
            plantId[eIdx] = this->spatialParams().rootParams(eIdx).plantId;
        }
    }

    /*!
     * \brief Capability to introduce problem-specific routines at the
     * beginning of the grid growth
     *
     * Function is called at the beginning of the standard grid
     * modification routine, GridGrowth::growGrid(.
     */
    void preGrowth()
    {
        ParentType::preGrowth();
        this->spatialParams().updateBeforeGrowth();
    }

    /*!
     * \brief Capability to introduce problem-specific routines at the
     * after of the grid growth
     *
     * Function is called after the standard grid
     * modification routine, GridGrowth::growGrid().
     */
    void postGrowth()
    {
        ParentType::postGrowth();
        // update root volume
        rootVolume_ = 0.0;
        for (const auto& element : elements(this->gridView()))
        {
            for (int plantId = 0; plantId < 2; ++plantId)
            {
                if (plantId == this->spatialParams().rootParams(element).plantId)
                {
                    const auto radius = this->spatialParams().rootParams(element).radius;
                    rootVolume_[plantId] += element.geometry().volume() * M_PI*radius*radius;
                }
            }
        }
    }

    //! return the pre growth volume of an scv
    //! needed for the residual for elements changing size by growth
    Scalar preGrowthVolume(const Element& element,
                           const SubControlVolume& scv,
                           const VolumeVariables& volVars)
    {
        if (this->isNew(element))
        {
            assert(std::abs(this->spatialParams().rootParams(element).previousLength) < 1e-13 && "Wrong root length");
            return 0.0;
        }
        else
        {
            assert(std::abs(this->spatialParams().rootParams(element).previousLength) > 1e-13 && "Wrong root length");
            return volVars.extrusionFactor() * this->spatialParams().rootParams(element).previousLength;
        }
    }

    //! return the post growth volume of an scv
    //! needed for the residual for elements changing size by growth
    Scalar postGrowthVolume(const Element& element,
                            const SubControlVolume& scv,
                            const VolumeVariables& volVars)
    {
        assert(std::abs(scv.volume() - this->spatialParams().rootParams(element).currentLength)< 1e-13  && "Wrong root length");
        return volVars.extrusionFactor()*scv.volume();
    }

    //! Called after every time step
    //! Output the total global exchange term
    void postTimeStep()
    {
        ParentType::postTimeStep();

        PrimaryVariables source(0.0);

        if (!(this->timeManager().time() < 0.0))
        {
            for (const auto& element : elements(this->gridView()))
            {
                auto fvGeometry = localView(this->model().fvGridGeometry());
                fvGeometry.bindElement(element);

                auto elemVolVars = localView(this->model().curGlobalVolVars());
                elemVolVars.bindElement(element, fvGeometry, this->model().curSol());

                for (auto&& scv : scvs(fvGeometry))
                {
                    auto pointSources = this->scvPointSources(element, fvGeometry, elemVolVars, scv);
                    pointSources *= scv.volume()*elemVolVars[scv].extrusionFactor();
                    source += pointSources;
                }
            }
        }

        std::cout << "Global integrated source (root): " << source << " (kg/s) / "
                  <<                           source*3600*24*1000 << " (g/day)" << '\n';

        std::vector<Scalar> segmentSizes(this->gridView().size(0));
        for (const auto& element : elements(this->gridView()))
            segmentSizes[this->elementMapper().index(element)] = element.geometry().volume();
        writeContainerToFile(segmentSizes, "segmentsizes.dat");
    }

    // don't write restart files
    bool writeRestartFile() const
    { return false; }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    //! Get the coupling manager
    CouplingManager& couplingManager()
    { return *couplingManager_; }

private:
    std::string name_;
    const Scalar eps_ = 1e-9;
    std::shared_ptr<CouplingManager> couplingManager_;
    Dune::FieldVector<Scalar, 2> rootVolume_;
};

} // end namespace Dumux

#endif
