// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Coupled root growth
 */
#ifndef DUMUX_ROSI_GROWTH_PROBLEM_HH
#define DUMUX_ROSI_GROWTH_PROBLEM_HH

#include "rootproblem.hh"
#include "soilproblem.hh"

#include <dumux/mixeddimension/problem.hh>
#include <dumux/mixeddimension/embedded/cellcentered/bboxtreecouplingmanager.hh>
#include <dumux/mixeddimension/embedded/cellcentered/bboxtreecouplingmanagersimple.hh>
#include <dumux/mixeddimension/integrationpointsource.hh>

#include <dumux/io/gnuplotinterface.hh>
#include "schursolver.hh"

namespace Dumux
{
template <class TypeTag>
class RosiGrowthProblem;

namespace Properties
{
NEW_TYPE_TAG(RosiGrowthProblem, INHERITS_FROM(MixedDimension));

// Set the problem property
SET_TYPE_PROP(RosiGrowthProblem, Problem, Dumux::RosiGrowthProblem<TypeTag>);

// Set the coupling manager
SET_TYPE_PROP(RosiGrowthProblem, CouplingManager, Dumux::CCBBoxTreeEmbeddedCouplingManagerSimple<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(RosiGrowthProblem, LowDimProblemTypeTag, TTAG(RootProblem));
SET_TYPE_PROP(RosiGrowthProblem, BulkProblemTypeTag, TTAG(SoilProblem));

// publish this problem in the sub problems
SET_TYPE_PROP(RootProblem, GlobalProblemTypeTag, TTAG(RosiGrowthProblem));
SET_TYPE_PROP(SoilProblem, GlobalProblemTypeTag, TTAG(RosiGrowthProblem));

// The subproblems inherit the parameter tree from this problem
SET_PROP(RootProblem, ParameterTree) : GET_PROP(TTAG(RosiGrowthProblem), ParameterTree) {};
SET_PROP(SoilProblem, ParameterTree) : GET_PROP(TTAG(RosiGrowthProblem), ParameterTree) {};

// Set the point source type of the subproblems to an integration point source
SET_TYPE_PROP(RootProblem, PointSource, Dumux::IntegrationPointSource<TTAG(RootProblem), unsigned int>);
SET_TYPE_PROP(RootProblem, PointSourceHelper, Dumux::IntegrationPointSourceHelper<TTAG(RootProblem)>);
SET_TYPE_PROP(SoilProblem, PointSource, Dumux::IntegrationPointSource<TTAG(SoilProblem), unsigned int>);
SET_TYPE_PROP(SoilProblem, PointSourceHelper, Dumux::IntegrationPointSourceHelper<TTAG(SoilProblem)>);

SET_TYPE_PROP(RosiGrowthProblem, LinearSolver, BlockILU0BiCGSTABSolver<TypeTag>);

}//end namespace properties

template <class TypeTag>
class RosiGrowthProblem : public MixedDimensionProblem<TypeTag>
{
    using ParentType = MixedDimensionProblem<TypeTag>;
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    // obtain the type tags of the sub problems
    using BulkProblemTypeTag = typename GET_PROP_TYPE(TypeTag, BulkProblemTypeTag);
    using LowDimProblemTypeTag = typename GET_PROP_TYPE(TypeTag, LowDimProblemTypeTag);

    // obtain types from the sub problem type tags
    using BulkProblem = typename GET_PROP_TYPE(BulkProblemTypeTag, Problem);
    using LowDimProblem = typename GET_PROP_TYPE(LowDimProblemTypeTag, Problem);

    using BulkGridView = typename GET_PROP_TYPE(BulkProblemTypeTag, GridView);
    using LowDimGridView = typename GET_PROP_TYPE(LowDimProblemTypeTag, GridView);

    using BulkPrimaryVariables = typename GET_PROP_TYPE(BulkProblemTypeTag, PrimaryVariables);
    using LowDimPrimaryVariables = typename GET_PROP_TYPE(LowDimProblemTypeTag, PrimaryVariables);

public:
    RosiGrowthProblem(TimeManager &timeManager, const BulkGridView &bulkGridView, const LowDimGridView &lowDimgridView)
    : ParentType(timeManager, bulkGridView, lowDimgridView)
    {
        checkMassConservation_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Problem, CheckMassConservation);
        enableGnuplotOutput_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Problem, GnuplotOutput);
    }

    void init()
    {
        ParentType::init();

        BulkPrimaryVariables bulkStorage(0.0);
        LowDimPrimaryVariables lowDimStorage(0.0);
        this->bulkProblem().model().globalStorage(bulkStorage);
        this->lowDimProblem().model().globalStorage(lowDimStorage);
        storageLastTimestep_ = bulkStorage[0] + lowDimStorage[0];
    }

    void preTimeStep()
    {
        ParentType::preTimeStep(); //empty but for iterativeSolver
        this->model().adaptVariableSize(); //adapts variable sizes of superproblem to lowdim and bulk size
    }

    //! If we should write a vtk file
    bool shouldWriteOutput() const
    { return true; }

    void postTimeStep()
    {
        ParentType::postTimeStep();

        if (checkMassConservation_)
        {
            // Do some checking of the mass conservation
            // The boundary fluxes have to be equal to the transpiration rate minus the change in storage
            // (1) storage change in kg/s
            BulkPrimaryVariables bulkStorage(0.0);
            LowDimPrimaryVariables lowDimStorage(0.0);
            this->bulkProblem().model().globalStorage(bulkStorage);
            this->lowDimProblem().model().globalStorage(lowDimStorage);
            auto newStorage = bulkStorage[0] + lowDimStorage[0];
            auto storageChange = (newStorage - storageLastTimestep_)/this->timeManager().timeStepSize();
            storageLastTimestep_ = newStorage;
            // (2) boundary fluxes in kg/s
            Scalar boundaryFluxes(0.0);
            // Calculate the mass inflow and outflow
            for (const auto& element : elements(this->bulkProblem().gridView()))
            {
                auto fvGeometry = localView(this->bulkProblem().model().fvGridGeometry());
                fvGeometry.bind(element);

                auto elemVolVars = localView(this->bulkProblem().model().curGlobalVolVars());
                elemVolVars.bind(element, fvGeometry, this->bulkProblem().model().curSol());

                auto elemFluxVarsCache = localView(this->bulkProblem().model().globalFluxVarsCache());
                elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

                for (const auto& scvf : scvfs(fvGeometry))
                    if(scvf.boundary())
                        boundaryFluxes += this->bulkProblem().model().localResidual().computeFlux(element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
            }

            // (3) transpiration rate in kg/s
            Scalar netTranspirationRate = 0.0;
            for (const auto& element : elements(this->lowDimProblem().gridView()))
            {
                auto fvGeometry = localView(this->lowDimProblem().model().fvGridGeometry());
                fvGeometry.bind(element);

                auto elemVolVars = localView(this->lowDimProblem().model().curGlobalVolVars());
                elemVolVars.bind(element, fvGeometry, this->lowDimProblem().model().curSol());

                for (const auto& scvf : scvfs(fvGeometry))
                    if(scvf.boundary())
                        netTranspirationRate += this->lowDimProblem().neumann(element, fvGeometry, elemVolVars, scvf)
                                                * elemVolVars[scvf.insideScvIdx()].extrusionFactor()
                                                * scvf.area();
            }

            auto balance = boundaryFluxes + storageChange + netTranspirationRate;
            std::cout << "boundary fluxes: " << boundaryFluxes << std::endl;
            std::cout << "storage change: " << storageChange << std::endl;
            std::cout << "transpiration rate: " << netTranspirationRate << std::endl;
            std::cout << "mass balance (bflux-dst-tr = 0): " << balance << std::endl;

            if (enableGnuplotOutput_)
            {
                // Gnuplot
                // variables that don't go out of scope
                static double yMax = std::numeric_limits<double>::lowest();
                static double yMin = 0;
                static std::vector<double> x, y, y2;

                static double accumulatedError = 0.0;
                auto balance_plot = balance;

                accumulatedError += balance_plot*this->timeManager().previousTimeStepSize();

                x.push_back(this->timeManager().time());
                y.push_back(balance_plot);
                y2.push_back(accumulatedError);

                yMax = std::max({yMax, accumulatedError, balance_plot});
                yMin =std::min({yMin, balance_plot, accumulatedError});

                gnuplot_.resetPlot();
                gnuplot_.setXRange(0, x.back()+1);
                gnuplot_.setYRange(yMin, yMax);
                gnuplot_.setXlabel("time [s]");
                gnuplot_.setYlabel("mass loss [kg]");
                gnuplot_.addDataSetToPlot(x, y, "balance", "axes x1y1 w l lw 4");
                gnuplot_.addDataSetToPlot(x, y2, "accumulatedError", "axes x1y1 w l lw 4");


                //plot all data interactively
                gnuplot_.plot("balance_plot");
            }
        }
    }

private:
    // the gnuplot output driver
    Dumux::GnuplotInterface<double> gnuplot_;
    bool enableGnuplotOutput_;

    Scalar storageLastTimestep_;
    bool checkMassConservation_;
};

} // end namespace Dumux

#endif
