// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GROWTH_DATATRANSFER_HELPER_DEFAULT_HH
#define DUMUX_GROWTH_DATATRANSFER_HELPER_DEFAULT_HH

#include <dune/grid/utility/persistentcontainer.hh>

/*!
 * \file
 * \brief Default implementation of user data transfer helper with growing grids
 */

namespace Dumux
{

template<class TypeTag>
class GrowthDataTransferHelperDefault
{
private:
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using Grid = typename GET_PROP_TYPE(TypeTag, Grid);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using PersistentContainer = Dune::PersistentContainer<Grid, PrimaryVariables>;

public:
    //! Constructor
    GrowthDataTransferHelperDefault(const GridView& gridView)
    : gridView_(gridView),
      data_(gridView.grid(), dofCodim) {}

    /*!
     * \brief Store data like primary variables and spatial parameters
     * \param problem The current problem
     */
    void storeData(Problem& problem)
    {
        // size the growthMap to the current size
        data_.resize();

        if(isBox)
            DUNE_THROW(Dune::NotImplemented, "Growth for box discretization!");
        else
        {
            for (const auto& element : elements(problem.gridView()))
            {
                // get reference to map entry
                auto& priVars = data_[element];

                // put your value in the map
                const unsigned int eIdx = problem.elementMapper().index(element);
                priVars = problem.model().curSol()[eIdx];
            }
        }
    }

    /*!
     * \brief Reconstruct missing primary variables and parameters (where elements are created/deleted)
     * \param problem The current problem
     */
    void reconstructData(Problem& problem)
    {
        data_.resize();

        if(isBox)
            DUNE_THROW(Dune::NotImplemented, "Growth for box discretization!");
        else
        {
            for (const auto& element : elements(problem.gridView()))
            {
                // old elements get their old variables assigned
                if(!element.isNew())
                {
                    auto& priVars = data_[element];
                    const unsigned int newEIdx = problem.elementMapper().index(element);
                    problem.model().curSol()[newEIdx] = priVars;
                }

                // initialize new elements with some value
                else
                {
                    const unsigned int newEIdx = problem.elementMapper().index(element);
                    problem.model().curSol()[newEIdx] = PrimaryVariables(0.0);
                }

            }
        }

        // reset entries in data map
        data_.resize(typename PersistentContainer::Value());
        data_.shrinkToFit();
        data_.fill(typename PersistentContainer::Value());
    }

private:
    GridView gridView_;
    PersistentContainer data_;
};

} // end namespace Dumux

#endif
