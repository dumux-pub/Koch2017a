// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for all growing problems
 */
#ifndef DUMUX_GROWTH_PROBLEM_HH
#define DUMUX_GROWTH_PROBLEM_HH

#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/growth/growth.hh>
#include <dumux/growth/properties.hh>

namespace Dumux
{
/*!
 * \ingroup GridGrowth
 * \ingroup ImplicitBaseProblems
 * \brief Base class for all growing problems
 */
template<class TypeTag>
class GrowthProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    using ParentType = ImplicitPorousMediaProblem<TypeTag>;
    using Implementation = typename GET_PROP_TYPE(TypeTag, Problem);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using Element = typename GridView::template Codim<0>::Entity;
    enum { growingGrid = GET_PROP_VALUE(TypeTag, GrowingGrid) };
    using GridGrowth = Dumux::GridGrowth<TypeTag>;

public:
    /*!
     * \brief The constructor.
     *
     * The overloaded class must allocate all data structures
     * required, but _must not_ do any calls to the model, the
     * jacobian assembler, etc inside the constructor.
     *
     * If the problem requires information from these, the
     * ImplicitProblem::init() method be overloaded.
     *
     * \param timeManager The TimeManager which keeps track of time
     * \param gridView The GridView used by the problem.
     */
    GrowthProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        clearIsNewMarkers();

        // if we are calculating on an growing grid get the grid growth model
        if (growingGrid)
            gridGrowth_ = std::make_shared<GridGrowth>(*static_cast<Implementation*>(this));

    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     *
     * If you overload this method don't forget to call
     * ParentType::init()
     */
    void init()
    {
        if (growingGrid) gridGrowth().init();
        this->spatialParams().init();
        ParentType::init();
    }

    /*!
     * \brief Called by the time manager after the time integration.
     */
    void preTimeStep()
    {
        ParentType::preTimeStep();
        clearIsNewMarkers();
        // If gridGrowth is used, this method grows the grid.
        // Remeber to call the parent class function if this is overwritten
        // on a lower problem level when using an adaptive grid
        if (growingGrid && this->timeManager().timeStepIndex() > 0)
        {
            this->gridGrowth().grow();

            // if the grid changed recompute the source map and the bounding box tree
            if (asImp_().gridChanged())
            {
                this->boundingBoxTree().build(this->gridView());
                asImp_().updatePointSources(); // can be overloaded in coupled problem, only needed in coupled
                this->computePointSourceMap_();
            }
        }
    }

    //! return the pre growth volume of an scv
    //! needed for the residual for elements changing size by growth
    Scalar preGrowthVolume(const Element& element,
                           const SubControlVolume& scv,
                           const VolumeVariables& volVars)
    {
        if (element.isNew())
            return 0.0;
        else
            return asImp_().postGrowthVolume(element, scv, volVars);
    }

    //! return the post growth volume of an scv
    //! needed for the residual for elements changing size by growth
    Scalar postGrowthVolume(const Element& element,
                            const SubControlVolume& scv,
                            const VolumeVariables& volVars)
    {
        return volVars.extrusionFactor()*scv.volume();
    }

    /*!
     * \brief Returns whether the grid has changed
     */
    bool gridChanged() const
    {
        if (growingGrid)
            return this->gridGrowth().hasGrown();
        else
            return false;
    }

    void updatePointSources()
    {}

    void clearIsNewMarkers()
    {
        isNew_.assign(this->gridView().size(0), false);
    }

    bool isNew(const Element &element) const
    {
        auto eIdx = this->elementMapper().index(element);
        return isNew_[eIdx];
    }

    void setIsNewMarker(const Element& element,
                        bool isNew)
    {
        auto eIdx = this->elementMapper().index(element);
        isNew_[eIdx] = isNew;
    }

    /*!
     * \brief Returns growth model used for the problem.
     */
    GridGrowth& gridGrowth()
    { return *gridGrowth_; }

    /*!
     * \brief Returns growth model used for the problem.
     */
    const GridGrowth& gridGrowth() const
    { return *gridGrowth_; }

    /*!
     * \brief Capability to introduce problem-specific routines at the
     * beginning of the grid growth
     *
     * Function is called at the beginning of the standard grid
     * modification routine, GridGrowth::growGrid(.
     */
    void preGrowth()
    {}

    /*!
     * \brief Capability to introduce problem-specific routines after grid growth
     *
     * Function is called at the end of the standard grid
     * modification routine, GridGrowth::growGrid() , to allow
     * for problem-specific output etc.
     */
    void postGrowth()
    {}

protected:

    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }


private:
    std::shared_ptr<GridGrowth> gridGrowth_;
    std::vector<bool> isNew_; //! Marker which element are currently new
};

} // end namespace Dumux

#endif
