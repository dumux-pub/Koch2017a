// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains growth laws for network growth,
 *        e.g. for linear or exponential growth.
 */
#ifndef DUMUX_GROWTHFUNCTION_HH
#define DUMUX_GROWTHFUNCTION_HH

#include <cmath>
#include <dune/common/exceptions.hh>

namespace Dumux
{

namespace GrowthModule
{

/*!
 * \brief Returns the growth length of linear growth
 *
 * \param rate the rate of growth
 * \param time the timestepsize
 * \param maxLength the maximal length of a root branch
 */
template<class Scalar>
inline Scalar linearGrowthLength(Scalar rate, Scalar time, Scalar maxLength)
{
    return std::min(maxLength, rate*time);
}

/*!
 * \brief Returns the growth length of linear growth
 *
 * \param rate the rate of growth
 * \param time the timestepsize
 * \param maxLength the maximal length of a root branch
 */
template<class Scalar>
inline Scalar linearGrowthAge(Scalar rate, Scalar length, Scalar maxLength)
{
    length = std::min(length, maxLength);
    return length/rate;
}

/*!
 * \brief Returns the growth length of exponential growth
 *
 * \param rate the rate of growth
 * \param time the timestepsize
 * \param lengthZero lenght before time step
 */
template<class Scalar>
inline Scalar exponentialGrowthLength(Scalar rate, Scalar time, Scalar maxLength)
{
    return maxLength*(1 - std::exp(-(rate/maxLength)*time));
}

/*!
 * \brief Returns the growth length of exponential growth
 *
 * \param rate the rate of growth
 * \param time the timestepsize
 * \param lengthZero lenght before time step
 */
template<class Scalar>
inline Scalar exponentialGrowthAge(Scalar rate, Scalar length, Scalar maxLength)
{
    length = std::min(length, maxLength);
    return -maxLength/rate*std::log(1-length/maxLength);
}

/*!
 * \brief Returns the growth length dependent on the type of growth
 * \param type the type of growth as a string
 * \param rate the rate of growth
 * \param time the timestepsize
 * \param maxLength the maximal length of a root branch
 */
template<class Scalar>
inline Scalar growthLength(const std::string& type, Scalar rate, Scalar time, Scalar maxLength)
{
    if (type == "linear")
        return linearGrowthLength(rate, time, maxLength);
    else if (type == "exponential")
        return exponentialGrowthLength(rate, time, maxLength);
    else
        DUNE_THROW(Dune::InvalidStateException, "Unknown growth function type: " << type);
}

/*!
 * \brief Returns the growth length dependent on the type of growth
 * \param type the type of growth as a string
 * \param rate the rate of growth
 * \param time the timestepsize
 * \param maxLength the maximal length of a root branch
 */
template<class Scalar>
inline Scalar growthAge(const std::string& type, Scalar rate, Scalar length, Scalar maxLength)
{
    if (type == "linear")
        return linearGrowthAge(rate, length, maxLength);
    else if (type == "exponential")
        return exponentialGrowthAge(rate, length, maxLength);
    else
        DUNE_THROW(Dune::InvalidStateException, "Unknown growth function type: " << type);
}

} // end namespace GridGrowth

} // end namespace Dumux

#endif
