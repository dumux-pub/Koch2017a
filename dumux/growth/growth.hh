// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for grid growth models.
 */
#ifndef DUMUX_GRIDGROWTH_HH
#define DUMUX_GRIDGROWTH_HH

#include <dune/common/timer.hh>

#include "properties.hh"

namespace Dumux
{

/*!\ingroup GridGrowth
 * \brief Standard Module for grid growth models.
 *
 * This class is created by the problem class with the template
 * parameters <TypeTag, true> and provides basic functionality
 * for grid growth methods:
 *
 * A standard implementation growGrid() will prepare everything
 * to calculate the next primary variables vector on the new grid.
 */
template<class TypeTag>
class GridGrowth
{
    // the growth policy class
    using GrowthAlgorithm = typename GET_PROP_TYPE(TypeTag, GrowthAlgorithm);

    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;

    enum {dim = GridView::dimension};
    enum {dimworld = GridView::dimensionworld};
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;

    static constexpr bool growingGrid = GET_PROP_VALUE(TypeTag, GrowingGrid);

public:
    /*!
     * Constructor
     * \param problem The problem
     */
    GridGrowth (Problem& problem)
    : problem_(problem),
      growthAlgorithm_(problem),
      hasGrown_(false)
    {}

    /*!
     * \brief Initalization of grid growth
     */
    void init()
    {
        growthAlgorithm_.init();
    }

    /*!
     * \brief Standard method for a growth step
     *
     * The implementation depends on the algorithm
     */
    void grow()
    {
        if (!growingGrid)
            return;

        // reset internal status
        hasGrown_ = false;

        Dune::Timer watch;

        std::cout << std::endl << "# Beginning growth step with dt = "
                  << problem_.timeManager().timeStepSize() << std::endl;

        // call the problem hook for all pre growth operations
        problem_.preGrowth();

        // grow using the chosen algorithm
        int elementsCreated, elementsRemoved, movedVertices;
        std::tie(elementsCreated, elementsRemoved, movedVertices) = growthAlgorithm_.grow();

        // call the problem hook for all post growth operations
        problem_.postGrowth();

        std::cout << "# Growth step created " << elementsCreated << " new elements"
                  << ", removed " << elementsRemoved << " elements"
                  << ", and moved " << movedVertices << " vertices in "
                  << watch.elapsed() << " seconds." << std::endl << std::endl;

        if (elementsCreated != 0 || elementsRemoved != 0 || movedVertices != 0)
            hasGrown_ = true;
    }

    bool hasGrown() const
    {
        return hasGrown_;
    }

private:
    Problem& problem_;
    GrowthAlgorithm growthAlgorithm_;
    bool hasGrown_;
};

/*!
 * \brief Default class for a static grid
 */
template<class TypeTag>
class NoGrowthAlgorithm
{
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
public:
    NoGrowthAlgorithm (Problem& problem) {}

    void init() {}

    std::pair<int, int> grow()
    { return std::make_pair<int, int>(0, 0); }
};

} // end namespace Dumux

#endif
