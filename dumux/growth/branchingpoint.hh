// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the branching point class needed for growth
 */
#ifndef DUMUX_GROWTH_BRANCHINGPOINT_HH
#define DUMUX_GROWTH_BRANCHINGPOINT_HH

namespace Dumux
{

namespace GrowthModule
{

// a point that is the origin of a future branch
// it contains the minimal information to start a new branch
template<class Scalar>
class BranchingPoint
{
public:
    BranchingPoint() : active_(false) {}

    void activate(unsigned int vIdxLocal, Scalar mainBranchLengthAtBirth)
    {
        vIdxLocal_ = vIdxLocal;
        mainBranchLengthAtBirth_ = mainBranchLengthAtBirth;
        active_ = true;
    }

    void deactivate()
    { active_ = false; }

    bool active() const
    { return active_; }

    unsigned int vIdxLocal() const
    { return vIdxLocal_; }

    Scalar mainBranchLengthAtBirth() const
    { return mainBranchLengthAtBirth_; }

private:
    unsigned int vIdxLocal_; // the vertex to grow out from
    Scalar mainBranchLengthAtBirth_; // at this main branch length the branch gets born
    bool active_;
};

} // end namespace GridGrowth

} // end namespace Dumux

#endif
