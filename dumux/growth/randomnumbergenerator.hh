// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GROWTH_RANDOM_NUMBER_GENERATOR_HH
#define DUMUX_GROWTH_RANDOM_NUMBER_GENERATOR_HH

#include <random>

namespace Dumux
{

template<class Scalar>
class NormalDistributedRandomNumber
{
public:
    NormalDistributedRandomNumber(Scalar mean, Scalar stddev)
    : gen_(r_()), dist_(mean, stddev) {}

    Scalar operator()()
    {
        return dist_(gen_);
    }

private:
    std::random_device r_;
    std::default_random_engine gen_;
    std::normal_distribution<Scalar> dist_;
};

template<class Scalar>
class UniformDistributedRandomNumber
{
public:
    UniformDistributedRandomNumber(Scalar min, Scalar max)
    : gen_(r_()), dist_(min, max) {}

    Scalar operator()()
    {
        return dist_(gen_);
    }

private:
    std::random_device r_;
    std::default_random_engine gen_;
    std::uniform_real_distribution<Scalar> dist_;
};

// for debugging (with valgrind) and reproducible random number generation
// template<class Scalar>
// class NormalDistributedRandomNumber
// {
// public:
//     NormalDistributedRandomNumber(Scalar mean, Scalar stddev)
//     : mean(mean), stddev(stddev) { std::srand(seed()); }

//     Scalar operator()()
//     {
//         //Box muller method (see http://stackoverflow.com/a/28551411)
//         static double n2 = 0.0;
//         static int n2_cached = 0;
//         if (!n2_cached)
//         {
//             double x, y, r;
//             do
//             {
//                 x = 2.0*rand()/RAND_MAX - 1;
//                 y = 2.0*rand()/RAND_MAX - 1;

//                 r = x*x + y*y;
//             }
//             while (r == 0.0 || r > 1.0);
//             {
//                 double d = sqrt(-2.0*log(r)/r);
//                 double n1 = x*d;
//                 n2 = y*d;
//                 double result = n1*stddev + mean;
//                 n2_cached = 1;
//                 return result;
//             }
//         }
//         else
//         {
//             n2_cached = 0;
//             return n2*stddev + mean;
//         }
//     }

//     //! Create reproducible random number sequences
//     int seed ()
//     {
//         static int seed = 0;
//         seed += 1;
//         return seed;
//     }

// private:
//     Scalar mean, stddev;
// };

// template<class Scalar>
// class UniformDistributedRandomNumber
// {
// public:
//     UniformDistributedRandomNumber(Scalar min, Scalar max)
//     : min(min), max(max) { std::srand(seed()); }

//     Scalar operator()()
//     {
//         auto random = (double) std::rand() / (((double)RAND_MAX) + 1.0) * (max-min+1) + min;
//         return random;
//     }

//     //! Create reproducible random number sequences
//     int seed ()
//     {
//         static int seed = 0;
//         seed += 1;
//         return seed;
//     }

// private:
//     Scalar min, max;
// };

} // end namespace Dumux

#endif
