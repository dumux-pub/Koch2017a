// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Adaptions to the model when using growing grids
 */

#ifndef DUMUX_GROWTH_MODEL_HH
#define DUMUX_GROWTH_MODEL_HH

#include <dumux/implicit/model.hh>
#include <dumux/growth/properties.hh>

#include <dune/istl/io.hh>

namespace Dumux
{
/*!
 * \ingroup GridGrowth
 * \brief Adaptions to the model when using growing grids
 */
template<class TypeTag >
class GrowthModel : public ImplicitModel<TypeTag>
{
    using ParentType = ImplicitModel<TypeTag>;

public:

    /*!
     * \brief Called by the update() method before it tries to
     *        apply the newton method.
     */
    void updateBegin()
    {
        ParentType::updateBegin();

        if(GET_PROP_VALUE(TypeTag, GrowingGrid) && this->problem_().gridChanged())
        {
            // update boundary indices
            this->updateBoundaryIndices_();

            // update the fv geometry
            this->fvGridGeometryPtr_->update(this->problem_());

            // resize and update the volVars with the initial solution
            this->curGlobalVolVars_.update(this->problem_(), this->curSol());

            // resize the matrix and assembly map if necessary
            this->localJacobian().init(this->problem_());
            this->jacobianAssembler().init(this->problem_());

            // update the flux variables caches
            this->globalfluxVarsCache_.update(this->problem_());

            // update the previous solution and volvars
            this->uPrev_ = this->uCur_;
            this->prevGlobalVolVars_ = this->curGlobalVolVars_;
       }
    }
};

} // end namespace Dumux

#include <dumux/growth/propertydefaults.hh>

#endif
