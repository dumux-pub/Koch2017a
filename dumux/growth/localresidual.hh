// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using growing grids
 */
#ifndef DUMUX_GROWTH_LOCAL_RESIDUAL_HH
#define DUMUX_GROWTH_LOCAL_RESIDUAL_HH

#include "properties.hh"
#include <dumux/implicit/localresidual.hh>

namespace Dumux
{
/*!
 * \ingroup RootsystemModel
 * \ingroup ImplicitLocalResidual
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the one-phase fully implicit model.
 */
template<class TypeTag>
class GrowthLocalResidual : public CCLocalResidual<TypeTag>
{
    using Implementation = typename GET_PROP_TYPE(TypeTag, LocalResidual);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using ElementBoundaryTypes = typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    using Element = typename GridView::template Codim<0>::Entity;

    //index of the mass balance equation
    enum {
        conti0EqIdx = Indices::conti0EqIdx //index for the mass balance
    };

public:

    void evalVolumeTerms_(const Element &element,
                          const FVElementGeometry& fvGeometry,
                          const ElementVolumeVariables& prevElemVolVars,
                          const ElementVolumeVariables& curElemVolVars,
                          const ElementBoundaryTypes &bcTypes)
    {
        // evaluate the volume terms (storage + source terms)
        for (auto&& scv : scvs(fvGeometry))
        {
            auto localScvIdx = 0; //isBox ? scv.index() : 0;

            const auto& curVolVars = curElemVolVars[scv];
            const auto& prevVolVars = prevElemVolVars[scv];

            // mass balance within the element. this is the
            // \f$\frac{m}{\partial t}\f$ term if using implicit
            // euler as time discretization.
            //
            // We might need a more explicit way for
            // doing the time discretization...
            PrimaryVariables curStorage = asImp_().computeStorage(scv, curVolVars);
            curStorage *= this->problem().postGrowthVolume(element, scv, curVolVars);
            this->storageTerm_[localScvIdx] = std::move(curStorage);

            PrimaryVariables prevStorage = asImp_().computeStorage(scv, prevVolVars);
            prevStorage *= this->problem().preGrowthVolume(element, scv, prevVolVars);
            this->storageTerm_[localScvIdx] -= std::move(prevStorage);

            this->storageTerm_[localScvIdx] /= this->problem().timeManager().timeStepSize();

            // add the storage term to the residual
            this->residual_[localScvIdx] += this->storageTerm_[localScvIdx];

            // subtract the source term from the local rate
            PrimaryVariables source = asImp_().computeSource(element, fvGeometry, curElemVolVars, scv);
            source *= scv.volume()*curVolVars.extrusionFactor();

            this->residual_[localScvIdx] -= source;
        }
    }

private:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! Returns the implementation of the problem (i.e. static polymorphism)
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }

};

} // end namespace Dumux

#endif
