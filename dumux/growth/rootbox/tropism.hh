// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The tropism class that decides whereto we grow
 */
#ifndef DUMUX_GROWTH_ROOTBOX_TROPISM_HH
#define DUMUX_GROWTH_ROOTBOX_TROPISM_HH

#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

#include <dumux/growth/parameters.hh>
#include <dumux/growth/randomnumbergenerator.hh>

namespace Dumux
{

namespace GrowthModule
{

// a point that is the origin of a future branch
// it contains the minimal information to start a new branch
template<class Scalar>
class Tropism
{
    using GlobalPosition = Dune::FieldVector<Scalar, 3>;
    using BranchParams = Dumux::GrowthModule::BranchParams<Scalar>;
    using BranchTypeParams = Dune::ParameterTree;
public:
    Tropism(const BranchParams& branchParams,
            const BranchTypeParams& branchTypeParams)
    : branchParams_(branchParams),
      tropismParams_(branchTypeParams.get<std::vector<Scalar>>("tropism", {0, 1, 0})),
      dice_(tropismParams_[1], tropismParams_[2]) {}

    GlobalPosition generateOptimalVector()
    {
        switch (int(std::round(tropismParams_[0])))
        {
            case 0: // No special tropism just random
                return generateRandomVector_();
            case 1: // Gravitropism (tendency to grow downwards)
                return GlobalPosition({0.0, 0.0, -1.0});
            case 2: // Plagitropism (tendency to grow horizontally)
            {
                auto p = generateRandomVector_();
                p[2] = 0.0;
                p /= p.two_norm();
                return p;
            }
            case 3: // Exotropism (tendency to follow the branch orientation)
                return branchParams_.orientation;
            case 4: // Mixed exo and gravitropism
            {
                auto a = branchParams_.orientation; a*= 0.7;
                auto b = GlobalPosition({0.0, 0.0, -0.3});
                auto p = a+b;
                p /= p.two_norm();
                return p;
            }
            default:
                DUNE_THROW(Dune::NotImplemented, "Tropism type " << tropismParams_[0]);
        }
    }

    int generateStrength()
    { return std::max(int(std::ceil(dice_())), 1); }

private:
    GlobalPosition generateRandomVector_()
    {
        UniformDistributedRandomNumber<Scalar> dice(-1.0, 1.0);
        GlobalPosition p({dice(), dice(), dice()});
        p /= p.two_norm();
        return p;
    }

    const BranchParams& branchParams_;
    std::vector<Scalar> tropismParams_; // {type, strength_mean, strength_stddev}
    NormalDistributedRandomNumber<Scalar> dice_;
};

} // end namespace GridGrowth

} // end namespace Dumux

#endif
