// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_ROOTBOX_GROWTH_DATA_TRANSFER_HELPER_HH
#define DUMUX_ROOTBOX_GROWTH_DATA_TRANSFER_HELPER_HH

#include <dune/grid/utility/persistentcontainer.hh>

#include <dumux/growth/parameters.hh>
#include <dumux/growth/branchingpoint.hh>

/*!
 * \file Data transfer helper for rootbox algorithm
 * \brief Data transfer helper for rootbox algorithm
 */

namespace Dumux
{

template<class TypeTag>
class RootBoxDataTransferHelper
{
private:
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using RootParams = Dumux::GrowthModule::RootParams<Scalar>;
    using BranchingPoint = Dumux::GrowthModule::BranchingPoint<Scalar>;

    struct Data
    {
        PrimaryVariables priVars;
        RootParams rootParams;
        BranchingPoint branchingPoint;
    };

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    using Grid = typename GridView::Grid;
    using LevelGridView = typename Grid::LevelGridView;
    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using PersistentContainer = Dune::PersistentContainer<Grid, Data>;

public:
    //! Constructs an adaptive helper object
    /**
     * In addition to providing a storage object for cell-centered Methods, this class provides
     * mapping functionality to adapt the grid.
     *
     *  @param gridView a DUNE gridview object corresponding to diffusion and transport equation
     */
    RootBoxDataTransferHelper(const GridView& gridView) :
        gridView_(gridView), data_(gridView.grid(), dofCodim)
    {}


    /*!
     * Store primary variables
     * \param problem The current problem
     */
    void storeData(Problem& problem)
    {
        data_.resize();

        if(isBox)
            DUNE_THROW(Dune::NotImplemented, "Growth for box discretization!");
        else
        {
            for (const auto& element : elements(problem.gridView()))
            {
                // get reference to map entry
                auto& priVars = data_[element].priVars;
                auto& rootParams = data_[element].rootParams;
                auto& branchingPoint = data_[element].branchingPoint;

                // put your value in the map
                const unsigned int eIdx = problem.elementMapper().index(element);
                priVars = problem.model().curSol()[eIdx];
                rootParams = problem.spatialParams().rootParams(element);
                branchingPoint = problem.spatialParams().branchingPoint(eIdx);
            }
        }
    }

    /*!
     * Reconstruct missing primary variables (where elements are created/deleted)
     * \param problem The current problem
     */
    void reconstructData(Problem& problem,
                         const std::vector<RootParams>& newRootParams,
                         const std::vector<BranchingPoint>& newBranchingPoints)
    {
        data_.resize();

        if(isBox)
            DUNE_THROW(Dune::NotImplemented, "Growth for box discretization!");
        else
        {
            problem.spatialParams().resize();
            for (const auto& element : elements(problem.gridView()))
            {
                // old elements get their old variables assigned
                if(!element.isNew())
                {
                    // get reference to map entry
                    auto& priVars = data_[element].priVars;
                    auto& rootParams = data_[element].rootParams;
                    auto& branchingPoint = data_[element].branchingPoint;

                    // get your value from the map
                    const unsigned int newEIdx = problem.elementMapper().index(element);
                    problem.model().curSol()[newEIdx] = priVars;
                    problem.spatialParams().rootParams(element) = rootParams;
                    problem.spatialParams().branchingPoint(newEIdx) = branchingPoint;
                }

                // initialize new elements with some value
                else
                {
                    problem.setIsNewMarker(element, true);
                    const unsigned int newEIdx = problem.elementMapper().index(element);
                    const unsigned int insertionIdx = problem.gridView().grid().growthInsertionIndex(element);
                    problem.spatialParams().rootParams(element) = newRootParams[insertionIdx];
                    problem.spatialParams().branchingPoint(newEIdx) = newBranchingPoints[insertionIdx];

                    // for the pressure we take that of the closest old element
                    // to this end we always walk up the branches in direction of the root collar
                    auto oldRootOrder = problem.spatialParams().rootParams(element).order;
                    auto oldElement = element;

                    while (oldElement.isNew())
                    {
                        for (const auto& intersection : intersections(problem.gridView(), oldElement))
                        {
                            if (intersection.neighbor())
                            {
                                // potential up-branch element
                                auto nextElement = intersection.outside();

                                // skip new neighbor elements on the same branch that were inserted later
                                // i.e. further towards the branch tip or if it's an element of a daughter branch
                                if (nextElement.isNew())
                                {
                                    const unsigned int nextIdx = problem.gridView().grid().growthInsertionIndex(element);

                                    if (nextIdx > insertionIdx)
                                        continue;

                                    // we get the order of a new element from the new root params
                                    if (newRootParams[nextIdx].order > oldRootOrder)
                                        continue;

                                    // we found the next up branch element
                                    oldRootOrder = newRootParams[nextIdx].order;
                                    oldElement = nextElement;
                                    break;
                                }

                                // skip old neighbor elements of daughter branches
                                else
                                {
                                    // we get the order of an old element from the persistent container
                                    if (data_[nextElement].rootParams.order > oldRootOrder)
                                        continue;

                                    // we found the final element we can extract the privars from
                                    oldRootOrder = data_[nextElement].rootParams.order;
                                    oldElement = nextElement;
                                    break;
                                }
                            }
                        }
                    }

                    // extract the privars from the closest old element we found above
                    problem.model().curSol()[newEIdx] = data_[oldElement].priVars;
                }
            }
        }

        // reset entries in restrictionmap
        data_.resize( typename PersistentContainer::Value() );
        data_.shrinkToFit();
        data_.fill( typename PersistentContainer::Value() );
    }

private:
    GridView gridView_;
    PersistentContainer data_;
};

} // end namespace Dumux

#endif
