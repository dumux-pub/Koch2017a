// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The growth algorithm like in RootBox
 */
#ifndef DUMUX_ROOTBOX_GROWTH_ALGORITHM_HH
#define DUMUX_ROOTBOX_GROWTH_ALGORITHM_HH

#include <tuple>

#include <dune/common/exceptions.hh>
#include <dune/common/timer.hh>

#include <dumux/growth/properties.hh>
#include <dumux/growth/parameters.hh>
#include <dumux/growth/randomnumbergenerator.hh>
#include <dumux/growth/growthfunction.hh>
#include <dumux/growth/rootbox/tropism.hh>
#include <dumux/growth/rootbox/datatransferhelper.hh>

namespace Dumux
{

/*!\ingroup GridGrowth
 * \brief The RootBox growth algorithm
 */
template<class TypeTag>
class RootBoxGrowthAlgorithm
{
    using GrowthHelper = typename GET_PROP_TYPE(TypeTag, GrowthDataTransferHelper);

    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Grid = typename GET_PROP_TYPE(TypeTag, Grid);

    enum {dim = GridView::dimension};
    enum {dimworld = GridView::dimensionworld};

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;

    using BranchParams = Dumux::GrowthModule::BranchParams<Scalar>;
    using RootParams = Dumux::GrowthModule::RootParams<Scalar>;
    using BranchingPoint = Dumux::GrowthModule::BranchingPoint<Scalar>;

    using Tropism = Dumux::GrowthModule::Tropism<Scalar>;

    //! A class providing all necessary information to grow / continue growing a root branch
    //! This is a temporary object created at beginning or during a growth step
    //! This used to convert the original RootBox recursive algorithm into
    //! a sequential one using a while loop and a stack of active branches
    class ActiveBranch
    {
    public:
        ActiveBranch() = default;

        ActiveBranch(unsigned int id,
               Scalar targetLength,
               unsigned int currentLocalVertexIdx,
               unsigned int currentElementIdx,
               const RootParams& currentTipRootParams,
               const Vertex& previousTipVertex)
        : id_(id),
          targetLength_(targetLength),
          previousTipVertex_(previousTipVertex),
          currentLocalVertexIdx_(currentLocalVertexIdx),
          currentElementIdx_(currentElementIdx),
          currentTipRootParams_(currentTipRootParams),
          tipFixed_(false)
        {}

        unsigned int id() const
        { return id_; }

        Scalar targetLength() const
        { return targetLength_; }

        unsigned int currentLocalVertexIdx() const
        { return currentLocalVertexIdx_; }

        unsigned int currentElementIdx() const
        { return currentElementIdx_; }

        const RootParams& currentTipRootParams() const
        { return currentTipRootParams_; }

        const Vertex& previousTipVertex() const
        { return previousTipVertex_; }

        bool tipFixed() const
        { return tipFixed_; }

        void fixTip()
        { return tipFixed_ = true; }

        void releaseTip()
        { return tipFixed_ = false; }

        void setCurrentLocalVertexIdx(unsigned int currentLocalVertexIdx)
        { currentLocalVertexIdx_ = currentLocalVertexIdx; }

        void setCurrentElementIdx(unsigned int currentElementIdx)
        { currentElementIdx_ = currentElementIdx; }

        void setCurrentTipRootParams(const RootParams& p)
        { currentTipRootParams_ = p; }

    private:
        const unsigned int id_; // a unique identifier
        const Scalar targetLength_; // the target length for the current growth step
        const Vertex previousTipVertex_; // the tip vertex that already existed before the growth step

        unsigned int currentLocalVertexIdx_; // the local index of the tip vertex within the tip element
        unsigned int currentElementIdx_; // could be an existing element or a new element
        RootParams currentTipRootParams_; // current tip root params (could be existing or new root params)
        bool tipFixed_; // if the tip is currently fixed and can't be moved because its a branching point
    };

public:
    /*!
     * Constructor
     * \param problem The problem
     */
    RootBoxGrowthAlgorithm (Problem& problem)
    : elementsCreated_(0), movedVertices_(0),
      problem_(problem),
      growthHelper_(problem.gridView())
    {}

    //! Initialization
    void init() {}

    /*!
     * \brief grows the grid
     * \note The actual growth algorithm happends in the growthStep method,
             then we need to store user data, grow the actual grid, and recover
             and initialize data on old and new elements.
     */
    std::tuple<int, int, int> grow()
    {
        //! (1.1) Stage elments for growth at root tips
        isBranchingPointVertex_.clear();
        isBranchingPointVertex_.resize(problem_.gridView().size(dim), false);

        elementsCreated_ = 0;
        movedVertices_ = 0;

        growthStep_();

        std::cout << "Growth step created " << elementsCreated_ << " elements and "
                  << "moved " << movedVertices_ << " vertices." << std::endl;

        //! (1.2) Store user data in a map
        growthHelper_.storeData(problem_);

        //! (1.3) Add the elements to the grid
        problem_.grid().preGrow();
        bool insertedElements = problem_.grid().grow();
        if (!insertedElements && elementsCreated_ > 0)
            std::cerr << "Warning: None of the elements could be inserted!" << std::endl;

        //! (1.4) Update mappers and solution vector
        problem_.elementMapper().update();
        problem_.vertexMapper().update();
        problem_.model().adaptVariableSize();
        problem_.clearIsNewMarkers();

        //! (1.5) Transfer data of old elements and initialize that of new ones
        growthHelper_.reconstructData(problem_, newRootParams_, newElementBranchingPoints_);

        //! (1.6) Delete the isNew markers, cleanup
        newRootParams_.clear();
        newElementBranchingPoints_.clear();
        problem_.grid().postGrow();

        return std::make_tuple(elementsCreated_, 0, movedVertices_);
    }

private:

    /*!
     * \brief The growth step. Growing new segments at the tips of the roots
     * \todo Doc me!
     */
    void growthStep_()
    {
        for (const auto& element : elements(problem_.gridView()))
        {
            // Check if we are a tip
            bool isTip = false;
            unsigned int tipIdxLocal = 0;

            for (const auto& intersection : intersections(problem_.gridView(), element))
            {
                const auto& globalPos = intersection.geometry().corner(0);
                // TODO scale the epsilon?
                if(intersection.boundary() && (globalPos[2] < problem_.bBoxMax()[2] - eps_ ))
                {
                    isTip = true;
                    tipIdxLocal = intersection.indexInInside();
                    break;
                }
            }

            // the element index
            auto eIdx = problem_.elementMapper().index(element);

            // Grow at tips
            if (isTip)
            {
                auto& segmentParams = problem_.spatialParams().rootParams(element);
                auto& branchParams = problem_.spatialParams().branchParams(segmentParams.branchId);
                // update tipIndexGlobal of the branch
                branchParams.tipIdxGlobal = problem_.vertexMapper().subIndex(element, tipIdxLocal, dim);

                // dead branches don't grow
                if (branchParams.alive)
                {
                    // Compute how far we are supposed to grow
                    const auto dt = problem_.timeManager().timeStepSize();
                    const auto age = dt + GrowthModule::growthAge(branchParams.growthFunction, branchParams.initialGrowthRate, branchParams.length, branchParams.maxLength);
                    const auto targetLength = GrowthModule::growthLength(branchParams.growthFunction, branchParams.initialGrowthRate, age, branchParams.maxLength);

                    // create an active branch object at this tip segment
                    ActiveBranch branch(segmentParams.branchId, targetLength, tipIdxLocal, eIdx,
                                        segmentParams, element.template subEntity<dim>(tipIdxLocal));

                    // queue the growing branch in the stack
                    branchStack_.push(std::move(branch));
                }
            }

            // ...and branching points of this element, if the time has come to branch out.
            auto& branchingPoint = problem_.spatialParams().branchingPoint(eIdx);
            if (branchingPoint.active())
            {
                auto& segmentParams = problem_.spatialParams().rootParams(element);
                auto& mainBranchParams = problem_.spatialParams().branchParams(segmentParams.branchId);
                // check if the branch is ready and create it if yes
                if (mainBranchParams.length > branchingPoint.mainBranchLengthAtBirth())
                {
                    // create new branch params for this new branch
                    auto branchType = problem_.spatialParams().getLateralBranchType(mainBranchParams.type);
                    auto branchTipIdxGlobal = problem_.vertexMapper().subIndex(element, branchingPoint.vIdxLocal(), dim);
                    auto geometry = element.geometry();
                    auto branchTipPosition = geometry.corner(branchingPoint.vIdxLocal());
                    auto parentBranchOrientation = branchTipPosition - geometry.corner(1-branchingPoint.vIdxLocal());
                    parentBranchOrientation /= parentBranchOrientation.two_norm();

                    auto branchId = problem_.spatialParams().createBranchParams(branchType,
                                                                                mainBranchParams.order+1,
                                                                                branchTipIdxGlobal,
                                                                                branchTipPosition,
                                                                                parentBranchOrientation);

                    //! Mark the vertex as immovable branching point
                    markAsBranchingPointVertex(branchTipIdxGlobal);

                    // the length increment for this time step, i.e. the target length since the current length is zero
                    const auto targetLength = mainBranchParams.length - branchingPoint.mainBranchLengthAtBirth();

                    // create a new branch
                    ActiveBranch branch(branchId, targetLength, tipIdxLocal, eIdx,
                                        segmentParams, element.template subEntity<dim>(tipIdxLocal));

                    // queue the new branch in the stack
                    branchStack_.push(std::move(branch));

                    // Deactivate the braching point, the branch will continue growing from its first element
                    branchingPoint.deactivate();
                }
            }

            // once the main branch structures where created... (either this is a tip of a branch growing or a new branch)
            // ...start the actual growth (this of course gets skipped if there is no branch in the stack for this element)
            while (!branchStack_.empty())
            {
                auto branch = branchStack_.top();
                branchStack_.pop();

                const auto& branchParamsBefore = problem_.spatialParams().branchParams(branch.id());
                Dune::dverb << "Begin to growth branch " << branch.id()
                            << " (type: "<< branchParamsBefore.type <<")"
                            << " from length->targetLength: " << branchParamsBefore.length << "->" << branch.targetLength() << std::endl;

                int branchElementsCreated, branchVerticesMoved;
                std::tie(branchElementsCreated, branchVerticesMoved) = growBranch_(branch);
                elementsCreated_ += branchElementsCreated;
                movedVertices_ += branchVerticesMoved;
            }
        }
    }

    //! Root growth algorithm (the root box algorithm)
    std::pair<int, int> growBranch_(ActiveBranch& branch)
    {
        // elements created on this branch (not including daughter branches)
        int branchElementsCreated = 0;
        int branchVerticesMoved = 0;

        // get target length
        const auto& targetLength = branch.targetLength();

        // get branch params
        auto& branchParams = problem_.spatialParams().branchParams(branch.id());

        // Update alive status of branch
        auto lengthAtDeath = GrowthModule::growthLength(branchParams.growthFunction, branchParams.initialGrowthRate, branchParams.lifeTime, branchParams.maxLength);
        if (targetLength > lengthAtDeath)
        {
            Dune::dverb << "  |-> Branch " << branch.id() << " died (lengthAtDeath = " << lengthAtDeath << ")" << std::endl;
            // this branch is now dead, we can stop here
            branchParams.alive = false;
            return std::make_pair(branchElementsCreated, branchVerticesMoved);
        }

        // compute the length increment for this growth step
        auto lengthIncrement = targetLength - branchParams.length;

        // if the length increment is too small
        // this means the time step size or the growth rate is extremely small
        // we say the change in structure is not detectable with our resolution
        // this effectively leads to growth in phases with no detectable activity and
        // phases with a detectable increase in length over a shorter time period
        // we have to neglect this error in contrast to continuous growth for sake of numerical stability
        if (lengthIncrement < zeroThreshold_)
            return std::make_pair(branchElementsCreated, branchVerticesMoved);

        // if this root branch has the ability to branch
        if (branchParams.numBranches > 0)
        {
            // if we are in the basal zone of the root branch
            if (branchParams.length < branchParams.basalZoneLength)
            {
                Dune::dverb << "  |-> Growing in basal zone... (basal zone length: " << branchParams.basalZoneLength << ")" << std::endl;
                // if growing doesn't get us out of the basal zone just grow at tip
                if (targetLength <= branchParams.basalZoneLength)
                {
                    int numSegments, numMovedVertices;
                    std::tie(numSegments, numMovedVertices) = createSegmentsForBranch_(branch, lengthIncrement, branchElementsCreated);
                    branchElementsCreated += numSegments;
                    branchVerticesMoved += numMovedVertices;
                    Dune::dverb << "  |-> Growing in basal zone: "<< lengthIncrement  << "->" << branchParams.length
                                << " (" << numSegments << " segments created)" << std::endl;
                    return std::make_pair(branchElementsCreated, branchVerticesMoved);
                }
                // if we grow out of the basal zone we grow until a branching point and
                // continue growing in the branching zone
                else
                {
                    auto distanceToEndOfBasalZone = branchParams.basalZoneLength - branchParams.length;
                    int numSegments, numMovedVertices;
                    std::tie(numSegments, numMovedVertices) = createSegmentsForBranch_(branch, distanceToEndOfBasalZone, branchElementsCreated);
                    branchElementsCreated += numSegments;
                    branchVerticesMoved += numMovedVertices;
                    lengthIncrement -= distanceToEndOfBasalZone;
                    Dune::dverb << "  |-> Growing in basal zone until first branching point: " << distanceToEndOfBasalZone << "->" << branchParams.length
                                << " (" << numSegments << " segments created)" << std::endl;

                    // if the target length is reached stop here
                    if (Dune::FloatCmp::eq(branchParams.length, targetLength, zeroThreshold_))
                        return std::make_pair(branchElementsCreated, branchVerticesMoved);
                }
            }

            // if we are in the branching zone or just reached it
            // because of growing out of the basal zone in this step
            if (branchParams.length >= branchParams.basalZoneLength)
            {
                Dune::dverb << "  |-> Growing in branching zone... (basal zone length: " << branchParams.basalZoneLength << ")" << std::endl;
                // grow and create branching points as long as we do not reach the target length
                auto nextBranchingPointLength = branchParams.basalZoneLength;
                for (const auto& interBranchDistance : branchParams.interBranchDistances)
                {
                    nextBranchingPointLength += interBranchDistance;
                    // if we already passed this braching point continue to the next branching point
                    if (branchParams.length >= nextBranchingPointLength)
                        continue;

                    // activate the branching point if it's not already activated
                    auto& branchingPoint = maybeActivateBranchingPoint_(branch.currentElementIdx(), branch.currentLocalVertexIdx(), branchParams, branchElementsCreated);
                    // then check if the new branching point is going to branch out within this growth step
                    if (targetLength > branchingPoint.mainBranchLengthAtBirth())
                    {
                        // create new branch params for this new branch
                        auto newBranchType = problem_.spatialParams().getLateralBranchType(branchParams.type);
                        auto newBranchId = problem_.spatialParams().createBranchParams(newBranchType,
                                                                                       branchParams.order+1,
                                                                                       branchParams.tipIdxGlobal,
                                                                                       branchParams.tipPosition,
                                                                                       branchParams.orientation);
                        //! Mark the vertex as immovable branching point
                        markAsBranchingPointVertex(branchParams.tipIdxGlobal);

                        // the length increment for this time step, i.e. the target length since the current length is zero
                        const auto newBranchTargetLength = targetLength - branchingPoint.mainBranchLengthAtBirth();

                        ActiveBranch newBranch(newBranchId, newBranchTargetLength,
                                               branch.currentLocalVertexIdx(),
                                               branch.currentElementIdx(),
                                               branch.currentTipRootParams(),
                                               branch.previousTipVertex());

                        // queue the new branch in the stack
                        branchStack_.push(std::move(newBranch));

                        // Deactivate the braching point, the branch will continue growing from its first element
                        branchingPoint.deactivate();
                    }

                    // ...then we continue growing
                    // if the target length is shorter that the length where the next branching point
                    // is going to be just grow the rest of the length increment and stop
                    if (targetLength <= nextBranchingPointLength)
                    {
                        int numSegments, numMovedVertices;
                        std::tie(numSegments, numMovedVertices) = createSegmentsForBranch_(branch, lengthIncrement, branchElementsCreated);
                        branchElementsCreated += numSegments;
                        branchVerticesMoved += numMovedVertices;
                        Dune::dverb << "  |-> Growing in branching zone between branches "<< lengthIncrement << "->" << branchParams.length
                                    << " (" << numSegments << " segments created)" << std::endl;
                        return std::make_pair(branchElementsCreated, branchVerticesMoved);
                    }

                    // if we are going to overstep another branching point grow exactly until that
                    // point and continue growing
                    else
                    {
                        auto distanceToNextBranchingPoint = nextBranchingPointLength - branchParams.length;
                        int numSegments, numMovedVertices;
                        std::tie(numSegments, numMovedVertices) = createSegmentsForBranch_(branch, distanceToNextBranchingPoint, branchElementsCreated);
                        branchElementsCreated += numSegments;
                        branchVerticesMoved += numMovedVertices;
                        Dune::dverb << "  |-> Growing in branching zone to next braching point " << lengthIncrement << "->" << branchParams.length
                                    << " (" << numSegments << " segments created)" << std::endl;
                        lengthIncrement -= distanceToNextBranchingPoint;

                        // if the target length is reached stop here
                        if (Dune::FloatCmp::eq(branchParams.length, targetLength, zeroThreshold_))
                            return std::make_pair(branchElementsCreated, branchVerticesMoved);
                    }
                }
                // if we still didn't reach the target length activate the last
                // branching point for this branch and continue growing in the apical zone if it's not already activated
                auto& branchingPoint = maybeActivateBranchingPoint_(branch.currentElementIdx(), branch.currentLocalVertexIdx(), branchParams, branchElementsCreated);
                if (targetLength > branchingPoint.mainBranchLengthAtBirth())
                {
                    // create new branch params for this new branch
                    auto newBranchType = problem_.spatialParams().getLateralBranchType(branchParams.type);
                    auto newBranchId = problem_.spatialParams().createBranchParams(newBranchType,
                                                                                   branchParams.order+1,
                                                                                   branchParams.tipIdxGlobal,
                                                                                   branchParams.tipPosition,
                                                                                   branchParams.orientation);

                    //! Mark the vertex as immovable branching point
                    markAsBranchingPointVertex(branchParams.tipIdxGlobal);

                    // the length increment for this time step, i.e. the target length since the current length is zero
                    const auto newBranchTargetLength = targetLength - branchingPoint.mainBranchLengthAtBirth();

                    ActiveBranch newBranch(newBranchId, newBranchTargetLength,
                                           branch.currentLocalVertexIdx(),
                                           branch.currentElementIdx(),
                                           branch.currentTipRootParams(),
                                           branch.previousTipVertex());

                    // queue the new branch in the stack
                    branchStack_.push(std::move(newBranch));

                    // Deactivate the braching point, the branch will continue growing from its first element
                    branchingPoint.deactivate();
                }
            }
            // if we still have a bit left to grow, grow the rest (we are in the apical zone)
            int numSegments, numMovedVertices;
            std::tie(numSegments, numMovedVertices) = createSegmentsForBranch_(branch, lengthIncrement, branchElementsCreated);
            branchElementsCreated += numSegments;
            branchVerticesMoved += numMovedVertices;
            Dune::dverb << "  |-> Growing in apical zone " << lengthIncrement << "->" << branchParams.length
                        << " (" << numSegments << " segments created)" << std::endl;
        }
        // this root branch doesn't have the ability to branch
        else
        {
            int numSegments, numMovedVertices;
            std::tie(numSegments, numMovedVertices) = createSegmentsForBranch_(branch, lengthIncrement, branchElementsCreated);
            branchElementsCreated += numSegments;
            branchVerticesMoved += numMovedVertices;
            Dune::dverb << "  |-> Growing a branch without branching ability " << lengthIncrement << "->" << branchParams.length
                        << " (" << numSegments << " segments created)" << std::endl;
        }

        return std::make_pair(branchElementsCreated, branchVerticesMoved);
    }

    //! Grow a single branch by length increment
    //! This might involve creating several segments if they would become too large otherwise
    std::pair<int, int> createSegmentsForBranch_(ActiveBranch& branch,
                                                 Scalar lengthIncrement,
                                                 int branchElementsCreated)
    {
        // if the length increment is too small
        // this means the time step size or the growth rate is extremely small
        // we say the change in structure is not detectable with our resolution
        // this effectively leads to growth in phases with no detectable activity and
        // phases with a detectable increase in length over a shorter time period
        // we have to neglect this error in contrast to continuous growth for sake of numerical stability
        if (lengthIncrement < zeroThreshold_)
            return std::make_pair(0, 0);

        auto& branchParams = problem_.spatialParams().branchParams(branch.id());
        const auto characteristicSegmentLength = problem_.spatialParams().characteristicSegmentLength();

        // if this is not the first segment of a new branch
        // we make sure first that the tip element of the current branch already reached the
        // characteristic segment length. If not, instead of inserting a new element we make
        // the existing tip element longer until it reaches the characteristic segment length.
        // (in constrast to the original rootbox algorithm this tries to not create a lot of small elements
        //  even for small time steps or low growth rates)
        // an exception are branching point which we keep fixed at their position
        int movedVertices = 0;
        // only do this for old elements in the branch, i.e. if no new element was added yet
        static const bool useMovingVertices = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Growth, UseMovingVertices);
        if (useMovingVertices)
        {
            if (branchElementsCreated == 0)
            {
                auto& rootParams = problem_.spatialParams().rootParams(branch.currentElementIdx());
                // skip this for the initial branch element
                // (this element will have the id of the mother branch. The id will get updated later)
                if(rootParams.branchId == branch.id() && rootParams.currentLength < characteristicSegmentLength)
                {
                    // if we are here we can be sure that the tip vertex already existed before this growth step
                    // skip if the tip vertex (of the last old branching segment) is an immovable branching point
                    if (!isBranchingPointVertex(branchParams.tipIdxGlobal))
                    {
                        // if the remaining length increment is larger than a small value we can start adding new segments
                        // if it is smaller (than 10% of the characteristic segment length)
                        // just add the bit to the tip element be done
                        const auto segmentLength = lengthIncrement <= 1.1*characteristicSegmentLength - rootParams.currentLength ?
                                                   lengthIncrement : characteristicSegmentLength - rootParams.currentLength;

                        auto newPoint = branchParams.tipPosition;
                        newPoint.axpy(segmentLength, branchParams.orientation); // keep the same orientation

                        // move the vertex (is that vertex always correct???)
                        assert(problem_.vertexMapper().index(branch.previousTipVertex()) == branchParams.tipIdxGlobal && "Wrong vertex");
                        problem_.grid().setPosition(branch.previousTipVertex(), newPoint);
                        branchParams.tipPosition = newPoint;

                        Dune::dverb << "-> Increased length (without adding segment) by: " << segmentLength;
                        Dune::dverb << ", old length: " << rootParams.currentLength;

                        // update the length info
                        rootParams.currentLength += segmentLength;
                        branchParams.length += segmentLength;

                        // update the length increment
                        lengthIncrement -= segmentLength;

                        Dune::dverb << ", new length " << rootParams.currentLength
                                    << ", remaining length increment: " << lengthIncrement << '\n';

                        if (Dune::FloatCmp::eq(lengthIncrement, 0.0, zeroThreshold_))
                            return std::make_pair(0, ++movedVertices);
                    }
                }
            }
        } // if useMovingVertices

        // make actual new segments for the remaining length increment
        unsigned int numSegments = std::max(1, static_cast<int>(std::round(lengthIncrement/characteristicSegmentLength)));
        const auto segmentLength = lengthIncrement/numSegments;
        auto predecessorSegmentParams = branch.currentTipRootParams();

        Dune::dverb << "-> Making " << numSegments << " new segments with length " << segmentLength << '\n';

        for (int segmentIdx = 0; segmentIdx < numSegments; ++segmentIdx)
        {
            // compute the point we are growing towards using random optimization
            const auto newPoint = getNewPoint_(branchParams, segmentLength);

            // create new segment to insert into grid
            auto newPointIdx = problem_.grid().insertVertex(newPoint);
            auto newElementIdx = problem_.grid().insertElement(Dune::GeometryType(1), {branchParams.tipIdxGlobal, newPointIdx});


            if ((newPoint - branchParams.tipPosition).two_norm() < zeroThreshold_)
            {
                std::cerr << "Warning: inserted a very small element with length: " << (newPoint - branchParams.tipPosition).two_norm() << std::endl;
                Dune::dverb << "-> The computed new point is: " << newPoint << std::endl;
                Dune::dverb << "-> The old tip position is: " << branchParams.tipPosition << std::endl;
                Dune::dverb << "-> The total planned length increase is: " << lengthIncrement << std::endl;
                Dune::dverb << "-> The segment's planned length increase is: " << segmentLength << std::endl;
                Dune::dverb << "-> This is segment " << segmentIdx+1 << "/" << numSegments << std::endl;
            }

            //! Make new root params for this segment
            newRootParams_.push_back(predecessorSegmentParams);
            //! Make new potential branching point
            newElementBranchingPoints_.push_back(BranchingPoint());

            //! Set the new root params
            auto& newRootParams = newRootParams_.back();

            //! Set the length for storage evaluation
            newRootParams.previousLength = 0.0;
            newRootParams.currentLength = segmentLength;

            // if this is the first element of a new brach
            // update branchId and other branch related params
            if (newRootParams.branchId != branch.id())
            {
                newRootParams.branchId = branch.id();
                newRootParams.order += 1;
                newRootParams.radius = branchParams.initialRadius;

                // make these new params the predecessorSegmentParams for the next segment
                predecessorSegmentParams = newRootParams;
            }

            // update the branch
            branch.setCurrentElementIdx(newElementIdx);
            branch.setCurrentLocalVertexIdx(1);
            branch.setCurrentTipRootParams(newRootParams);

            // increase the branch length
            branchParams.length += segmentLength;

            // update the tip information
            branchParams.orientation = newPoint - branchParams.tipPosition;
            branchParams.orientation /= branchParams.orientation.two_norm();
            branchParams.tipPosition = newPoint;
            branchParams.tipIdxGlobal = newPointIdx;
        }

        return std::make_pair(numSegments, movedVertices);
    }

    // add a new braching point for the current newest element
    BranchingPoint& maybeActivateBranchingPoint_(unsigned int eIdx,
                                                 unsigned int tipIdxLocal,
                                                 const BranchParams& branchParams,
                                                 int branchElementsCreated)
    {
        auto mainBranchLengthAtBirth = branchParams.length + branchParams.apicalZoneLength;
        // if this is a tip for which the element already exists in the grid
        if (branchElementsCreated == 0)
        {
            if (!problem_.spatialParams().branchingPoint(eIdx).active())
            {
                problem_.spatialParams().branchingPoint(eIdx).activate(tipIdxLocal, mainBranchLengthAtBirth);
                Dune::dverb << "  |-> Activated branching point at: " << branchParams.length << std::endl;
            }
            return problem_.spatialParams().branchingPoint(eIdx);
        }
        else
        {
            if (!newElementBranchingPoints_[eIdx].active())
            {
                newElementBranchingPoints_[eIdx].activate(tipIdxLocal, mainBranchLengthAtBirth);
                Dune::dverb << "  |-> Activated branching point at: " << branchParams.length << std::endl;
            }
            return newElementBranchingPoints_[eIdx];
        }
    }

    GlobalPosition getNewPoint_(const BranchParams& branchParams,
                                Scalar lengthIncrement)
    {
        UniformDistributedRandomNumber<Scalar> dice(-1.0, 1.0);

        GrowthModule::Tropism<Scalar> tropism(branchParams, problem_.spatialParams().branchTypeParams(branchParams.type));
        const auto optimalVector = tropism.generateOptimalVector();

        GlobalPosition newPoint;
        auto minAngle = std::numeric_limits<Scalar>::max();

        int n = tropism.generateStrength();
        for (int i=0; i<n; ++i)
        {
            // create a random vector
            GlobalPosition p({dice(), dice(), dice()});
            p /= p.two_norm();

            // if this plant grows in a confined space check if this
            // new point would be possible
            // we use the maximum length since this segment might grow more in the next growth step
            auto maxP = branchParams.tipPosition;
            maxP.axpy(1.1*problem_.spatialParams().characteristicSegmentLength(), p);
            if(!problem_.pointInDomain(maxP))
            {
                ++n; // increase realizations by one since this one was invalid
                Dune::dverb << " New point outside domain. Throw dice again!" << std::endl;
                continue;
            }

            // compute actual potential new point by walking length increment in direction
            // of new orientation vector from the tip position
            p *= lengthIncrement;
            p += branchParams.tipPosition;

            // check if angle to optimal vector is smaller than before
            auto angle = std::abs(std::acos(optimalVector*p));
            assert(std::isfinite(angle));

            if (angle < minAngle)
            {
                newPoint = p;
                minAngle = angle;
            }
        }
        return newPoint;
    }

    //! Mark a vertex as immovable branching point
    void markAsBranchingPointVertex(unsigned int i)
    {
        // we only need to mark vertices that already existed
        if (i < isBranchingPointVertex_.size())
            isBranchingPointVertex_[i] = true;
    }

    //! If a vertex is an immovable branching point
    bool isBranchingPointVertex(unsigned int i)
    {
        assert(i < isBranchingPointVertex_.size());
        return isBranchingPointVertex_[i];
    }

private:
    int elementsCreated_;
    int movedVertices_;

    Problem& problem_;
    GrowthHelper growthHelper_;

    //! the stack of active branches that are growing in the current growth step
    std::stack<ActiveBranch> branchStack_;
    //! if we create a new segment we push back the new elements' branching points in here
    std::vector<BranchingPoint> newElementBranchingPoints_;
    //! if we create a new segment we push back the new elements' parameters in here
    std::vector<RootParams> newRootParams_;

    //! if the vertex can't be move because it's a branching point
    std::vector<bool> isBranchingPointVertex_;

    static constexpr Scalar eps_ = 1.5e-7;
    static constexpr Scalar zeroThreshold_ = 1e-10;
};

} // end namespace Dumux

#endif
