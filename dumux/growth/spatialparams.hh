// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters class for the root system test problem
 */
#ifndef DUMUX_GROWTH_SPATIALPARAMS_HH
#define DUMUX_GROWTH_SPATIALPARAMS_HH

#include <dune/common/parametertreeparser.hh>

#include <dumux/material/spatialparams/implicit1p.hh>
#include <dumux/material/components/simpleh2o.hh>

namespace Dumux
{

/*!
 * \ingroup GridGrowth
 *
 * \brief Definition of the spatial parameters for root systems with growth
 */
template<class TypeTag>
class RootGrowthSpatialParams: public ImplicitSpatialParamsOneP<TypeTag>
{
    using ParentType = ImplicitSpatialParamsOneP<TypeTag>;
    using Implementation = typename GET_PROP_TYPE(TypeTag, SpatialParams);
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    using GridCreator = typename GET_PROP_TYPE(TypeTag, GridCreator);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using SubControlVolume = typename GET_PROP_TYPE(TypeTag, SubControlVolume);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using ElementSolutionVector = typename GET_PROP_TYPE(TypeTag, ElementSolutionVector);
    using Indices = typename GET_PROP_TYPE(TypeTag, Indices);
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld
    };
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;

    // Map from position in the dgf file to parameter name
    enum DGFParams {
        orderIdx = 0,
        branchIdIdx = 1,
        surfaceIdx = 2,
        massIdx = 3,
        plantIdx = 5
    };

    using BranchParams = Dumux::GrowthModule::BranchParams<Scalar>;
    using RootParams = Dumux::GrowthModule::RootParams<Scalar>;
    using BranchingPoint = Dumux::GrowthModule::BranchingPoint<Scalar>;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    RootGrowthSpatialParams(const Problem& problem, const GridView& gridView)
        : ParentType(problem, gridView), gridView_(gridView), problemPtr_(&problem)
    {
        // read the root growth runtime params from file
        const auto rootParamFile = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, RootParameterFile);
        Dune::ParameterTreeParser::readINITree(rootParamFile, runTimeGrowthParams_, false);

        constantKx_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Kx);
        constantKr_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Kr);
        characteristicSegmentLength_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Growth, CharacteristicSegmentLength);
    }

    void init()
    {
        resize();
        getRootParamsFromDGF_();
        // Dont't forget to initialize branch params in the actual problem's spatial params
    }

    void resize()
    {
        branchParams_.reserve(5000); // maximum number of branches, over this number the
                                     // vector will be realloated leading to segmentation faults
        rootParams_.resize(gridView_.size(0));
        branchingPoints_.resize(gridView_.size(0));
    }

    //! Todo maybe set to scv.volume?
    void updateBeforeGrowth()
    {
        for (auto&& rootParams : rootParams_)
            rootParams.previousLength = rootParams.currentLength;
    }

    //! //////////////////////////////////////////////////////////
    //! (Individual) branch parameters and branch type parameters
    //! //////////////////////////////////////////////////////////

    //! Create new individual branch parameters when the plant branched somewhere
    unsigned int createBranchParams(const std::string& branchType,
                                    unsigned int branchOrder,
                                    unsigned int branchTipIdxGlobal,
                                    const GlobalPosition& branchTipPosition,
                                    const GlobalPosition& parentBranchOrientation)
    {
        branchParams_.emplace_back(branchType, branchOrder, branchTypeParams(branchType),
                                   branchTipIdxGlobal, branchTipPosition, parentBranchOrientation);
        return branchParams_.size()-1;
    }

    //! return branchParams
    BranchParams& branchParams(unsigned int branchId)
    {
        assert(branchId < branchParams_.size());
        return branchParams_[branchId];
    }

    //! returnbranchParams
    const BranchParams& branchParams(unsigned int branchId) const
    {
        assert(branchId < branchParams_.size());
        return branchParams_[branchId];
    }

    //! Get all branch type parameters of a specific branchType
    const Dune::ParameterTree& branchTypeParams(const std::string& branchType)
    {
        return runTimeGrowthParams_.sub(branchType);
    }

    std::string getLateralBranchType(const std::string& branchType)
    {
        const auto& typeParams = branchTypeParams(branchType);
        auto successors = typeParams.template get<std::vector<std::string>>("successors", std::vector<std::string>());
        if (successors.empty())
            return "None";
        else if (successors.size() == 1)
            return successors[0];
        else
        {
            // roll the dice with successorProbabilities
            auto successorProbabilities = typeParams.template get<std::vector<Scalar>>("successorP");
            if (successorProbabilities.empty())
                return successors[0];
            else
            {
                //create random number between 0 and 1
                Scalar rand0 = rand() % 10;
                Scalar d = rand0/10;
                Scalar sumProbabilities = std::accumulate(successorProbabilities.begin(), successorProbabilities.end(), 0.0);
                //normalize in case probabilities do not add up to 1
                std::transform(successorProbabilities.begin(), successorProbabilities.end(), successorProbabilities.begin(),  std::bind1st(std::multiplies<Scalar>(), (1/sumProbabilities)));

                //add probability each step and compare with random number
                Scalar successorProbabilityAccumulated = 0;
                successorProbabilityAccumulated += successorProbabilities[0];
                Scalar i = 0;
                while (successorProbabilityAccumulated <= d)
                {
                    i ++;
                    successorProbabilityAccumulated += successorProbabilities[i];
                }
                 return successors[i];
            }
        }
    }

    //! ///////////////////////////////////////////////////////////////
    //! Branching points. Points that are origin of a new future brach
    //! ///////////////////////////////////////////////////////////////
    BranchingPoint& branchingPoint(unsigned int eIdx)
    {
        return branchingPoints_[eIdx];
    }

    //! //////////////////////////////////////////////////////////
    //! Individual root segment parameters
    //! //////////////////////////////////////////////////////////

    RootParams& rootParams(const Element &element)
    {
        auto eIdx = problem().elementMapper().index(element);
        return rootParams_[eIdx];
    }

    const RootParams& rootParams(const Element &element) const
    {
        auto eIdx = problem().elementMapper().index(element);
        return rootParams_[eIdx];
    }

    RootParams& rootParams(unsigned int eIdx)
    {
        return rootParams_[eIdx];
    }

    const RootParams& rootParams(unsigned int eIdx) const
    {
        return rootParams_[eIdx];
    }

    //! The characteristic segment length when inserting new elements
    //! \note It is guaranteed that most elements in the grid will be in the size
    //!       of the characteristic segment length +/-10%.
    Scalar characteristicSegmentLength() const
    {
        return characteristicSegmentLength_;
    }

    //! //////////////////////////////////////////////////////////
    //! Root parameters for the flow simulation
    //! //////////////////////////////////////////////////////////

    /*!
     * \brief Function for defining the root radius.
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return rootRadius
     */
    Scalar radius(const Element &element) const
    {
        auto eIdx = problem().elementMapper().index(element);
        return rootParams_[eIdx].radius;
    }

    Scalar radius(int eIdx) const
    {
        return rootParams_[eIdx].radius;
    }

    //! the problem
    const Problem& problem() const
    { return *problemPtr_; }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The element
     * \param scv The sub control volume
     * \param elemSol The element solution vector
     * \return the porosity
     */
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolutionVector& elemSol) const
    { return 0.4; }

    /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param globalPos The global position
     */
    Scalar solidHeatCapacityAtPos(const GlobalPosition& globalPos) const
    { return 790; /*specific heat capacity of granite [J / (kg K)]*/ }

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param globalPos The global position
     */
    Scalar solidDensityAtPos(const GlobalPosition& globalPos) const
    { return 2700; /*density of granite [kg/m^3]*/ }

    /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the solid
     *
     * This is only required for non-isothermal models.
     *
     * \param globalPos The global position
     */
    Scalar solidThermalConductivityAtPos(const GlobalPosition& globalPos) const
    { return 2.8; }

private:
    //! Read the params for the initial grid
    void getRootParamsFromDGF_()
    {
        for (const auto& element : elements(gridView_))
        {
            auto eIdx = problem().elementMapper().index(element);

            // in case of adaptive grids
            Element level0eIt(element);
            for(auto levelIdx = element.level(); levelIdx != 0; levelIdx--)
                level0eIt = level0eIt.father();

            Scalar rootLength = element.geometry().volume();
            Scalar rootSurface = GridCreator::parameters(level0eIt)[DGFParams::surfaceIdx]/(1 << element.level());

            //! Set the root params for this element
            rootParams_[eIdx].radius = rootSurface / rootLength / 2.0 / M_PI; // assume circular cross-section
            rootParams_[eIdx].order = GridCreator::parameters(level0eIt)[DGFParams::orderIdx];
            rootParams_[eIdx].branchId = GridCreator::parameters(level0eIt)[DGFParams::branchIdIdx]-1; // we start with 0
            rootParams_[eIdx].axialPerm = constantKx_;
            rootParams_[eIdx].radialPerm = constantKr_;

            if (GridCreator::parameters(level0eIt).size() == 6)
                rootParams_[eIdx].plantId = GridCreator::parameters(level0eIt)[DGFParams::plantIdx];

            rootParams_[eIdx].currentLength = rootLength;
            rootParams_[eIdx].previousLength = rootLength;
        }
    }

    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

    GridView gridView_;
    const Problem* problemPtr_;

    Scalar constantKx_;
    Scalar constantKr_;
    Scalar characteristicSegmentLength_;

    std::vector<BranchParams> branchParams_;
    std::vector<RootParams> rootParams_;
    std::vector<BranchingPoint> branchingPoints_;

    GrowthModule::RuntimeParams runTimeGrowthParams_;
};

} // end namespace Dumux

#endif
