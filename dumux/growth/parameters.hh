// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains parameter classes needed for growth
 */
#ifndef DUMUX_GROWTH_PARAMETERS_HH
#define DUMUX_GROWTH_PARAMETERS_HH

#include <dune/common/parametertree.hh>
#include <dumux/common/math.hh>
#include <dumux/growth/randomnumbergenerator.hh>
#include <dumux/growth/growthfunction.hh>

namespace Dumux
{

namespace GrowthModule
{

//!< Parameters given for every root segment
template <class Scalar>
struct RootParams
{
    Scalar radius;
    Scalar axialPerm;
    Scalar radialPerm;
    int order;
    int branchId; // which branch this segment is belonging to
    int plantId; // which plant this segment is belonging to
    Scalar currentLength; // current length
    Scalar previousLength; // length before last growth step
};

//!< Parameters given for every branch / root
template <class Scalar>
struct BranchParams
{
private:
    using GlobalPosition = Dune::FieldVector<Scalar, 3>;
    using NormalDistributedRandomNumber = Dumux::NormalDistributedRandomNumber<Scalar>;
    using UniformDistributedRandomNumber = Dumux::UniformDistributedRandomNumber<Scalar>;
public:
    // construct with a branch type and order
    BranchParams(const std::string& branchType, unsigned int branchOrder,
                 const Dune::ParameterTree& branchTypeParams,
                 unsigned int branchTipIdxGlobal,
                 const GlobalPosition& branchTipPosition,
                 const GlobalPosition& parentBranchOrientation)
    : type(branchType),
      order(branchOrder),
      length(+0.0),
      alive(true),
      tipIdxGlobal(branchTipIdxGlobal),
      tipPosition(branchTipPosition)
    {
        // create individual branch parameters with distribution given per branchtype
        // TODO convert parameter files to better names

        // basal zone length
        {
        auto params = branchTypeParams.get<std::vector<Scalar>>("lb");
        NormalDistributedRandomNumber dice(params[0], params[1]);
        basalZoneLength = std::max(dice(), 1e-6);
        }

        // apical zone length
        {
        auto params = branchTypeParams.get<std::vector<Scalar>>("la");
        NormalDistributedRandomNumber dice(params[0], params[1]);
        apicalZoneLength = std::max(dice(), 0.0);
        }

        // maximum number of branches
        {
        auto params = branchTypeParams.get<std::vector<Scalar>>("nob");
        NormalDistributedRandomNumber dice(params[0], params[1]);
        numBranches = std::max(int(dice()), 0);
        }

        // inter branch distances
        {
        interBranchDistances.resize(numBranches);
        auto params = branchTypeParams.get<std::vector<Scalar>>("ln");
        NormalDistributedRandomNumber dice(params[0], params[1]);
        maxLength = 0.0;
        for (auto&& d : interBranchDistances)
        {
            d = std::max(dice(), 1e-6);
            maxLength += d;
        }
        maxLength += apicalZoneLength + basalZoneLength;
        }

        // initial growth rate
        {
        auto params = branchTypeParams.get<std::vector<Scalar>>("r");
        NormalDistributedRandomNumber dice(params[0], params[1]);
        initialGrowthRate = std::max(dice(), 0.0);
        }

        // initial radius
        {
        auto params = branchTypeParams.get<std::vector<Scalar>>("a");
        NormalDistributedRandomNumber dice(params[0], params[1]);
        initialRadius = std::max(dice(), 1e-6);
        }

        // root life time
        {
        auto params = branchTypeParams.get<std::vector<Scalar>>("rlt");
        NormalDistributedRandomNumber dice(params[0], params[1]);
        lifeTime = std::max(dice(), 0.0);
        }

        // initial angle
        {
        auto params = branchTypeParams.get<std::vector<Scalar>>("theta");
        NormalDistributedRandomNumber dice(params[0], params[1]);
        auto theta = std::max(dice(), 0.0);

        // compute the initial orientation
        // pick randomly one of the vector that has an angle of theta with the parent orientation vector
        UniformDistributedRandomNumber dice2(0.0, 1.0);
        auto randomVector = GlobalPosition({dice2(), dice2(), dice2()});
        auto tangentialVector = parentBranchOrientation;
        auto perpendicularVector = crossProduct(tangentialVector, randomVector);
        // to make this robust we have to check if either the random vector or the perpenduclar vector have length zero
        while (perpendicularVector.two_norm() < 1e-10 || randomVector.two_norm() < 1e-10)
        {
            randomVector = GlobalPosition({dice2(), dice2(), dice2()});
            perpendicularVector = crossProduct(tangentialVector, randomVector);
        }
        perpendicularVector /= perpendicularVector.two_norm();
        perpendicularVector *= std::sin(theta);
        tangentialVector *= std::cos(theta);
        orientation = tipPosition + tangentialVector + perpendicularVector;
        }
        //growthFunction
        growthFunction =  branchTypeParams.get<std::string>("gf");
    };

    std::string type;
    unsigned int order;
    Scalar length; // current length
    bool alive;
    Scalar maxLength;
    Scalar lifeTime;

    unsigned int tipIdxGlobal;
    GlobalPosition tipPosition;
    GlobalPosition orientation;

    Scalar basalZoneLength;
    Scalar apicalZoneLength;
    int numBranches;
    std::vector<Scalar> interBranchDistances;

    Scalar initialGrowthRate;
    Scalar initialRadius;
    std::string growthFunction;
};

//!< All parameters given at runtime via a parameter file (ends with *.rparam)
//! \note This includes root system params, branch type params, other growth algorithm params
//! \note For now this is just a Dune::ParameterTree
using RuntimeParams = Dune::ParameterTree;

} // end namespace GridGrowth

} // end namespace Dumux

#endif
