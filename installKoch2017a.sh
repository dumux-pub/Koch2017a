# DUNE/DUMUX VERSIONS
# dune-common
# master # 39752738226aa53881415baeba99485e6458b505 # 2017-08-30 14:57:29 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout master
git reset --hard 39752738226aa53881415baeba99485e6458b505
cd ..

# dune-foamgrid
# cleanup/get-referenceelement-copies #  #  #
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout master
git reset --hard a34764afadcb1cd2e2f2e3675a6356e252d76718
cd ..

# dune-geometry
# master # d8c6c94f66a62366eea81cd1a7d08e6506063be9 # 2017-08-30 16:28:00 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout master
git reset --hard d8c6c94f66a62366eea81cd1a7d08e6506063be9
cd ..

# dune-grid
# master # 4096e3929ad0c798431c5d41fa0de1cf24c175e0 # 2017-08-30 18:32:45 +0000 # Andreas Dedner
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout master
git reset --hard 4096e3929ad0c798431c5d41fa0de1cf24c175e0
cd ..

# dune-istl
# feature/fgmres # 1d014192c7d6c3c915263b20cf230b565cf547f8 # 2017-06-05 17:31:21 +0200 # Timo Koch
git clone https://gitlab.dune-project.org/tkoch/dune-istl.git
cd dune-istl
git checkout feature/fgmres
git reset --hard 1d014192c7d6c3c915263b20cf230b565cf547f8
cd ..

# dune-localfunctions
# master # 914b3535abda23501f0225c40b129b67a6733651 # 2017-08-30 16:35:34 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout master
git reset --hard 914b3535abda23501f0225c40b129b67a6733651
cd ..

# dune-uggrid
# master # d593bf38f69cba96ace385a4fb92b772e3b8f90c # 2017-09-01 10:56:21 +0000 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/staging/dune-uggrid.git
cd dune-uggrid
git checkout master
git reset --hard d593bf38f69cba96ace385a4fb92b772e3b8f90c
cd ..

# dumux
# feature/extendend-richards # f1d6896e99e7e8cc6f066fd0414391f199bb73c2 # 2017-09-04 10:14:55 +0200 # Timo Koch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout feature/extendend-richards
git reset --hard f1d6896e99e7e8cc6f066fd0414391f199bb73c2
cd ..
